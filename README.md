# Object Model Service

## Docs
- [Usage guide](/docs/usage-guide.md) contains the description of OMS grpc calls
- [Contract on event service topics](/docs/topics-contract.md)

- [Development tips](/docs/development.md)
- [Linting tips](/docs/lint.md)

## TODO

- [x] Convert postgres errors to custom rpc errors
- [ ] All postgres update queries use `coalesce`
- [ ] Database
  - [ ] Soft deletion
- [x] GRPC some timestamps are invalid due to null value in database
- [x] Redis
 - [x] protocol
 - [x] implementation
- [x] buf
 - [x] pushes to repository
 - [x] package documentation


- [ ] Postgres database init with correct locale
- [ ] Lock guard's section
- [ ] Refactoring
- [ ] Proto file updates
  - [ ] User.Lang uses enumeration
