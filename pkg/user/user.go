package user

import (
	"context"
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"
	"gitlab.com/m11259/object-model-service/internal/pg"
	"gitlab.com/m11259/object-model-service/proto/gen/oms/v1"
	"gitlab.com/m11259/object-model-service/internal/utils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	proto.UnimplementedUserServiceServer
	queries *pg.Queries
	log     *logrus.Entry
}

func New(queries *pg.Queries, l *logrus.Entry) *Server {
	return &Server{
		queries: queries,
		log:     l,
	}
}

func pgToProtoUsers(users []pg.User) []*proto.User {
	protoUsers := make([]*proto.User, len(users))

	for i, u := range users {
    updated := u.UpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
    username := u.Username.String
    lastname := u.Lastname.String
		protoUsers[i] = &proto.User{
			Id:               u.ID,
			Username:         &username,
			Telegramname:     u.Telegramname,
			Firstname:        u.Firstname,
			Lastname:         &lastname,
			Phone:            u.Phone,
			Address:          u.Address,
			CarNumbers:       u.CarNumbers,
			AddressConfirmed: u.AddressConfirmed,
			Identified:       u.Identified.Bool,
			Activated:        u.Activated.Bool,
			Lang:             u.Lang,
      CreatedAt:        u.CreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
      UpdatedAt:        &updated,
		}
	}

	return protoUsers
}

func (s *Server) CreateUser(ctx context.Context, req *proto.CreateUserRequest) (*proto.User, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	lang := req.GetLang()
	if lang == "" {
		lang = "uk"
	}

  userParams := pg.CreateUserParams{
		ID: req.GetId(),
		Username: sql.NullString{
			String: req.GetUsername(),
			Valid:  req.GetUsername() != "",
		},
		Telegramname: req.GetTelegramname(),
		Firstname:    req.GetFirstname(),
		Lastname: sql.NullString{
			String: req.GetLastname(),
			Valid:  req.GetLastname() != "",
		},
		Phone:            req.GetPhone(),
		Address:          req.GetAddress(),
		AddressConfirmed: req.GetAddressConfirmed(),
		Activated: sql.NullBool{
			Bool:  req.GetActivated(),
			Valid: true,
		},
		Identified: sql.NullBool{
			Bool:  req.GetIdentified(),
			Valid: true,
		},
		CarNumbers: req.GetCarNumbers(),
		Lang:       lang,
	}

	user, err := s.queries.CreateUser(ctx, userParams)

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.WithField("user", user).Info(`Created user`)

  updated := user.UpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
  username := user.Username.String
  lastname := user.Lastname.String
	return &proto.User{
		Id:               user.ID,
		Username:         &username,
		Telegramname:     user.Telegramname,
		Firstname:        user.Firstname,
		Lastname:         &lastname,
		Phone:            user.Phone,
		Address:          user.Address,
		CarNumbers:       user.CarNumbers,
		AddressConfirmed: user.AddressConfirmed,
		Identified:       user.Identified.Bool,
		Activated:        user.Activated.Bool,
		Lang:             lang,
    CreatedAt:        user.CreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
    UpdatedAt:        &updated,
	}, nil
}

func (s *Server) GetAllUsers(ctx context.Context, req *proto.GetAllUsersRequest) (*proto.UsersPage, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	users, err := s.queries.GetUsers(ctx, pg.GetUsersParams{
		Limit:  req.Limit,
		Offset: req.Offset,
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

	return &proto.UsersPage{
		Users: pgToProtoUsers(users),
	}, nil
}

func (s *Server) GetUnidentifiedUsers(ctx context.Context, req *proto.GetUnidentifiedUsersRequest) (*proto.UsersPage, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	users, err := s.queries.GetUnidentifiedUsers(ctx, pg.GetUnidentifiedUsersParams{
		Limit:  req.Limit,
		Offset: req.Offset,
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

	return &proto.UsersPage{
		Users: pgToProtoUsers(users),
	}, nil
}

func (s *Server) GetUser(ctx context.Context, req *proto.GetUserRequest) (*proto.User, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	if req.Id == nil && req.Username == nil {
    l.Info(`id and username are not specified`)
		return nil, errors.New("id and username are not specified")
	}

	var user pg.User

	var err error

	if req.Id != nil {
		user, err = s.queries.GetUserById(ctx, req.GetId())
	} else if req.Username != nil {
		user, err = s.queries.GetUserByUsername(ctx, sql.NullString{
			String: req.GetUsername(),
			Valid:  true,
		})
	}

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  updated := user.UpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
  username := user.Username.String
  lastname := user.Lastname.String
	return &proto.User{
		Id:               user.ID,
		Username:         &username,
		Telegramname:     user.Telegramname,
		Firstname:        user.Firstname,
		Lastname:         &lastname,
		Phone:            user.Phone,
		Address:          user.Address,
		CarNumbers:       user.CarNumbers,
		AddressConfirmed: user.AddressConfirmed,
		Identified:       user.Identified.Bool,
		Activated:        user.Activated.Bool,
		Lang:             user.Lang,
    CreatedAt:        user.CreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
    UpdatedAt:        &updated,
	}, nil
}

func (s *Server) RetrieveUsers(ctx context.Context, req *proto.RetrieveUsersRequest) (*proto.UsersPage, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	users, err := s.queries.RetrieveUsers(ctx, pg.RetrieveUsersParams{
		Limit:            req.GetLimit(),
		Offset:           req.GetOffset(),
		Username:         sql.NullString{String: req.GetUsernameSubstring(), Valid: req.UsernameSubstring != nil},
		Telegramname:     sql.NullString{String: req.GetTelegramnameSubstring(), Valid: req.TelegramnameSubstring != nil},
		Firstname:        sql.NullString{String: req.GetFirstnameSubstring(), Valid: req.FirstnameSubstring != nil},
		Lastname:         sql.NullString{String: req.GetLastnameSubstring(), Valid: req.LastnameSubstring != nil},
		Phone:            sql.NullString{String: req.GetPhoneSubstring(), Valid: req.PhoneSubstring != nil},
		Address:          sql.NullString{String: req.GetAddressSubstring(), Valid: req.AddressSubstring != nil},
		AddressConfirmed: sql.NullBool{Bool: req.GetAddressConfirmed(), Valid: req.AddressConfirmed != nil},
		CarNumbers:       req.GetCarNumbers(),
		Identified:       sql.NullBool{Bool: req.GetIdentified(), Valid: req.Identified != nil},
		Activated:        sql.NullBool{Bool: req.GetActivated(), Valid: req.Activated != nil},
		Lang:             sql.NullString{String: req.GetLang(), Valid: req.Lang != nil},
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.WithField("num", len(users)).Debug("retrieved users")

	return &proto.UsersPage{
		Users: pgToProtoUsers(users),
	}, nil
}

func (s *Server) UpdateUser(ctx context.Context, req *proto.UpdateUserRequest) (*proto.User, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	user, err := s.queries.UpdateUser(ctx, pg.UpdateUserParams{
		ID: req.GetId(),
		Firstname: sql.NullString{
			String: req.GetFirstname(),
			Valid:  req.Firstname != nil,
		},
		Lastname: sql.NullString{
			String: req.GetLastname(),
			Valid:  req.Lastname != nil,
		},
		Phone: sql.NullString{
			String: req.GetPhone(),
			Valid:  req.Phone != nil,
		},
		Address: sql.NullString{
			String: req.GetAddress(),
			Valid:  req.Address != nil,
		},
		AddressConfirmed: sql.NullBool{
			Bool:  req.GetAddressConfirmed(),
			Valid: req.AddressConfirmed != nil,
		},
		Identified: sql.NullBool{
			Bool:  req.GetIdentified(),
			Valid: req.Identified != nil,
		},
		Activated: sql.NullBool{
			Bool:  req.GetActivated(),
			Valid: req.Activated != nil,
		},
		Lang: sql.NullString{
			String: req.GetLang(),
			Valid:  req.Lang != nil,
		},
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  updated := user.UpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
  username := user.Username.String
  lastname := user.Lastname.String
	return &proto.User{
		Id:               user.ID,
		Username:         &username,
		Telegramname:     user.Telegramname,
		Firstname:        user.Firstname,
		Lastname:         &lastname,
		Phone:            user.Phone,
		Address:          user.Address,
		CarNumbers:       user.CarNumbers,
		AddressConfirmed: user.AddressConfirmed,
		Identified:       user.Identified.Bool,
		Activated:        user.Activated.Bool,
		Lang:             user.Lang,
    CreatedAt:        user.CreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
    UpdatedAt:        &updated,
	}, nil
}

func (s *Server) DeleteUser(ctx context.Context, req *proto.DeleteUserRequest) (*proto.User, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	user, err := s.queries.DeleteUser(ctx, req.GetId())

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  updated := user.UpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
  username := user.Username.String
  lastname := user.Lastname.String
	return &proto.User{
		Id:               user.ID,
		Username:         &username,
		Telegramname:     user.Telegramname,
		Firstname:        user.Firstname,
		Lastname:         &lastname,
		Phone:            user.Phone,
		Address:          user.Address,
		CarNumbers:       user.CarNumbers,
		AddressConfirmed: user.AddressConfirmed,
		Identified:       user.Identified.Bool,
		Activated:        user.Activated.Bool,
		Lang:             user.Lang,
    CreatedAt:        user.CreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
    UpdatedAt:        &updated,
	}, nil
}
