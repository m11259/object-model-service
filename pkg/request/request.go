package request

import (
	"context"
	"database/sql"
	"time"

	"github.com/jackc/pgtype"
	"github.com/sirupsen/logrus"
	"gitlab.com/m11259/object-model-service/internal/pg"
	"gitlab.com/m11259/object-model-service/proto/gen/oms/v1"
	"gitlab.com/m11259/object-model-service/internal/utils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	proto.UnimplementedRequestServiceServer
	queries *pg.Queries
	log     *logrus.Entry
}

func New(queries *pg.Queries, l *logrus.Entry) *Server {
	return &Server{
		queries: queries,
		log:     l,
	}
}

func pgRequestToProto(request pg.Request, user *pg.User) proto.Request {
	var protoUser *proto.User
	if user != nil {
    u := user.UpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
		protoUser = &proto.User{
			Id:               user.ID,
			Username:         &user.Username.String,
			Telegramname:     user.Telegramname,
			Firstname:        user.Firstname,
			Lastname:         &user.Lastname.String,
			Phone:            user.Phone,
			Address:          user.Address,
			CarNumbers:       user.CarNumbers,
			AddressConfirmed: user.AddressConfirmed,
			Identified:       user.Identified.Bool,
			Activated:        user.Activated.Bool,
			Lang:             user.Lang,
      CreatedAt:        user.CreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
      UpdatedAt:        &u,
		}
	}

	var pass *string

	if request.Pass.Status == pgtype.Present {
		s := string(request.Pass.Bytes)
		pass = &s
	}

	var reviewedAt *string
	if request.ReviewedAt.Valid {
    s := request.ReviewedAt.Time.Format("2006-01-02T15:04:05.999Z")
    reviewedAt = &s
	}

	var fulfilledAt *string
	if request.FulfilledAt.Valid {
    s := request.FulfilledAt.Time.Format("2006-01-02T15:04:05.999Z")
    fulfilledAt = &s
	}

  created := request.CreatedAt.Format("2006-01-02T15:04:05.999Z")
	return proto.Request{
		Id:          request.ID,
		Body:        request.Body,
		Address:     request.UserAddress.String,
		CarNumber:   &request.CarNumber.String,
		User:        protoUser,
		Status:      request.Status,
		Pass:        pass,
    CreatedAt:   &created,
		ReviewedAt:   reviewedAt,
		FulfilledAt: fulfilledAt,
	}
}

func pgRequestsToProto(requestsPg []*pg.Request) []*proto.Request {
  return nil
}

func (s *Server) CreateRequest(ctx context.Context, req *proto.CreateRequestRequest) (*proto.Request, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	request, err := s.queries.CreateRequest(ctx, pg.CreateRequestParams{
		UserID: sql.NullInt64{
			Int64: req.GetUserId(),
			Valid: req.UserId != nil,
		},
		Body: req.GetBody(),
		UserAddress: sql.NullString{
			String: req.GetAddress(),
			Valid:  true,
		},
		CarNumber: sql.NullString{
			String: req.GetCarNumber(),
			Valid:  req.CarNumber != nil,
		},
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Debug(`Requst created`)

	protoRequest := pgRequestToProto(request, nil)

	return &protoRequest, nil
}

func (s *Server) RetrieveRequests(ctx context.Context, req *proto.RetrieveRequestsRequest) (*proto.RequestList, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  pgRequestParams := pg.RetrieveRequestsParams{
    Limit: req.Limit,
    Offset: req.Offset,
  	UserID:              sql.NullInt64{Int64: req.GetUserId(), Valid: req.UserId != nil},
  	BodyContains:        sql.NullString{String: req.GetBodyContains(), Valid: req.BodyContains != nil},
  	UserAddressContains: sql.NullString{String: req.GetUserAddressSubstring(), Valid: req.UserAddressSubstring != nil},
  	UserAddress:         sql.NullString{String: req.GetUserAddress(), Valid: req.UserAddress != nil},
  	CarNumberContains:   sql.NullString{String: req.GetCarNumberSubstring(), Valid: req.CarNumberSubstring != nil},
  	CarNumber:           sql.NullString{String: req.GetCarNumber(), Valid: req.CarNumber != nil},
  	Status:              sql.NullInt32{Int32: req.GetStatus(), Valid: req.Status != nil,},
  }

  if req.CreatedAtFrom != nil && req.CreatedAtTo != nil {
    tf, err := time.Parse("2006-01-02T15:04:05.999Z", req.GetCreatedAtFrom())
    if err != nil {
      return nil, status.Error(codes.InvalidArgument, "invalid CreatedAtFrom")
    }
    tt, err := time.Parse("2006-01-02T15:04:05.999Z", req.GetCreatedAtTo())
    if err != nil {
      return nil, status.Error(codes.InvalidArgument, "invalid CreatedAtTo")
    }
    pgRequestParams.CreatedAtFrom = sql.NullTime{ Time: tf, Valid: true }
    pgRequestParams.CreatedAtTo = sql.NullTime{ Time: tt, Valid: true }
  }
  if req.ReviewedAtFrom != nil && req.ReviewedAtTo != nil {
    tf, err := time.Parse("2006-01-02T15:04:05.999Z", req.GetReviewedAtFrom())
    if err != nil {
      return nil, status.Error(codes.InvalidArgument, "invalid ReviewedAtFrom")
    }
    tt, err := time.Parse("2006-01-02T15:04:05.999Z", req.GetReviewedAtTo())
    if err != nil {
      return nil, status.Error(codes.InvalidArgument, "invalid ReviewedAtTo")
    }
    pgRequestParams.ReviewedAtFrom = sql.NullTime{ Time: tf, Valid: true }
    pgRequestParams.ReviewedAtTo = sql.NullTime{ Time: tt, Valid: true }
  }
  if req.FulfilledAtFrom != nil && req.FulfilledAtTo != nil {
    tf, err := time.Parse("2006-01-02T15:04:05.999Z", req.GetFulfilledAtFrom())
    if err != nil {
      return nil, status.Error(codes.InvalidArgument, "invalid FulfilledAtFrom")
    }
    tt, err := time.Parse("2006-01-02T15:04:05.999Z", req.GetFulfilledAtTo())
    if err != nil {
      return nil, status.Error(codes.InvalidArgument, "invalid FulfilledAtTo")
    }
    pgRequestParams.FulfilledAtFrom = sql.NullTime{ Time: tf, Valid: true }
    pgRequestParams.FulfilledAtTo = sql.NullTime{ Time: tt, Valid: true }
  }

  requestsPg, err := s.queries.RetrieveRequests(ctx, pgRequestParams)

  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  requests := make([]*proto.Request, len(requestsPg))
  var carNumber *string
  var user *proto.User
  var pass *string
  for i, rrr := range requestsPg {
    if rrr.CarNumber.Valid {
      s := rrr.CarNumber.String
      carNumber = &s
    } else {
      carNumber = nil
    }
    if rrr.UserID.Valid {
      var updated *string
      if rrr.UserUpdatedAt.Valid {
        s := rrr.UserUpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
        updated = &s
      }
      user = &proto.User{
      	Id:               rrr.UserID.Int64,
      	Username:         &rrr.UserUsername.String,
      	Telegramname:     rrr.UserTelegramname.String,
      	Firstname:        rrr.UserFirstname.String,
      	Lastname:         &rrr.UserLastname.String,
      	Phone:            rrr.UserPhone.String,
      	Address:          rrr.UserAddress.String,
      	CarNumbers:       rrr.UserCarNumbers,
      	AddressConfirmed: rrr.UserAddressConfirmed.Bool,
      	Identified:       rrr.UserIdentified.Bool,
      	Activated:        rrr.UserActivated.Bool,
      	Lang:             rrr.UserLang.String,
        CreatedAt:        rrr.UserCreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
      	UpdatedAt:        updated,
      }
    } else {
      user = nil
    }
    if rrr.Pass.Status == pgtype.Present {
      pss := string(rrr.Pass.Bytes)
      pass = &pss
    } else {
      pass = nil
    }

    var reviewed *string
    if rrr.ReviewedAt.Valid {
      s := rrr.ReviewedAt.Time.Format("2006-01-02T15:04:05.999Z")
      reviewed = &s
    }
    var fulfilled *string
    if rrr.FulfilledAt.Valid {
      s := rrr.FulfilledAt.Time.Format("2006-01-02T15:04:05.999Z")
      fulfilled = &s
    }
    created := rrr.CreatedAt.Format("2006-01-02T15:04:05.999Z")
    requests[i] = &proto.Request{
    	Id:          rrr.ID,
    	Body:        rrr.Body,
    	Address:     rrr.Address.String,
    	CarNumber:   carNumber,
    	User:        user,
    	Status:      rrr.Status,
    	Pass:        pass,
      CreatedAt:   &created,
    	ReviewedAt:   reviewed,
    	FulfilledAt: fulfilled,
    }
  }
  return &proto.RequestList{
  	Requests: requests,
  }, nil
}


func (s *Server) GetPendingRequests(ctx context.Context, req *proto.GetPendingRequestsRequest) (*proto.RequestList, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  requestsPg, err := s.queries.GetRequestsPending(ctx)

  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  requests := make([]*proto.Request, len(requestsPg))
  var carNumber *string
  var user *proto.User
  var pass *string
  for i, rrr := range requestsPg {
    if rrr.CarNumber.Valid {
      s := rrr.CarNumber.String
      carNumber = &s
    } else {
      carNumber = nil
    }
    if rrr.UserID.Valid {
      var updated *string
      if rrr.UserUpdatedAt.Valid {
        s := rrr.UserUpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
        updated = &s
      }
      user = &proto.User{
      	Id:               rrr.UserID.Int64,
      	Username:         &rrr.UserUsername.String,
      	Telegramname:     rrr.UserTelegramname.String,
      	Firstname:        rrr.UserFirstname.String,
      	Lastname:         &rrr.UserLastname.String,
      	Phone:            rrr.UserPhone.String,
      	Address:          rrr.UserAddress.String,
      	CarNumbers:       rrr.UserCarNumbers,
      	AddressConfirmed: rrr.UserAddressConfirmed.Bool,
      	Identified:       rrr.UserIdentified.Bool,
      	Activated:        rrr.UserActivated.Bool,
      	Lang:             rrr.UserLang.String,
        CreatedAt:        rrr.UserCreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
      	UpdatedAt:        updated,
      }
    } else {
      user = nil
    }
    if rrr.Pass.Status == pgtype.Present {
      pss := string(rrr.Pass.Bytes)
      pass = &pss
    } else {
      pass = nil
    }

    var reviewed *string
    if rrr.ReviewedAt.Valid {
      s := rrr.ReviewedAt.Time.Format("2006-01-02T15:04:05.999Z")
      reviewed = &s
    }
    var fulfilled *string
    if rrr.FulfilledAt.Valid {
      s := rrr.FulfilledAt.Time.Format("2006-01-02T15:04:05.999Z")
      fulfilled = &s
    }
    created := rrr.CreatedAt.Format("2006-01-02T15:04:05.999Z")
    requests[i] = &proto.Request{
    	Id:          rrr.ID,
    	Body:        rrr.Body,
    	Address:     rrr.Address.String,
    	CarNumber:   carNumber,
    	User:        user,
    	Status:      rrr.Status,
    	Pass:        pass,
      CreatedAt:   &created,
    	ReviewedAt:   reviewed,
    	FulfilledAt: fulfilled,
    }
  }

  l.WithFields(logrus.Fields{
    "num": len(requests),
  }).Debug("retrieved pending requests")

  return &proto.RequestList{
  	Requests: requests,
  }, nil
}

func (s *Server) GetReviewedRequests(ctx context.Context, req *proto.GetReviewedRequestsRequest) (*proto.RequestList, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  requestsPg, err := s.queries.GetRequestsReviewed(ctx, pg.GetRequestsReviewedParams{
  	Limit:  req.Limit,
  	Offset: req.Offset,
  })

  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  requests := make([]*proto.Request, len(requestsPg))
  var carNumber *string
  var user *proto.User
  var pass *string
  for i, rrr := range requestsPg {
    if rrr.CarNumber.Valid {
      s := rrr.CarNumber.String
      carNumber = &s
    } else {
      carNumber = nil
    }
    if rrr.UserID.Valid {
      var updated *string
      if rrr.UserUpdatedAt.Valid {
        s := rrr.UserUpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
        updated = &s
      }
      user = &proto.User{
      	Id:               rrr.UserID.Int64,
      	Username:         &rrr.UserUsername.String,
      	Telegramname:     rrr.UserTelegramname.String,
      	Firstname:        rrr.UserFirstname.String,
      	Lastname:         &rrr.UserLastname.String,
      	Phone:            rrr.UserPhone.String,
      	Address:          rrr.UserAddress.String,
      	CarNumbers:       rrr.UserCarNumbers,
      	AddressConfirmed: rrr.UserAddressConfirmed.Bool,
      	Identified:       rrr.UserIdentified.Bool,
      	Activated:        rrr.UserActivated.Bool,
      	Lang:             rrr.UserLang.String,
        CreatedAt:        rrr.UserCreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
      	UpdatedAt:        updated,
      }
    } else {
      user = nil
    }
    if rrr.Pass.Status == pgtype.Present {
      pss := string(rrr.Pass.Bytes)
      pass = &pss
    } else {
      pass = nil
    }

    var reviewed *string
    if rrr.ReviewedAt.Valid {
      s := rrr.ReviewedAt.Time.Format("2006-01-02T15:04:05.999Z")
      reviewed = &s
    }
    var fulfilled *string
    if rrr.FulfilledAt.Valid {
      s := rrr.FulfilledAt.Time.Format("2006-01-02T15:04:05.999Z")
      fulfilled = &s
    }
    created := rrr.CreatedAt.Format("2006-01-02T15:04:05.999Z")
    requests[i] = &proto.Request{
    	Id:          rrr.ID,
    	Body:        rrr.Body,
    	Address:     rrr.Address.String,
    	CarNumber:   carNumber,
    	User:        user,
    	Status:      rrr.Status,
    	Pass:        pass,
      CreatedAt:   &created,
    	ReviewedAt:   reviewed,
    	FulfilledAt: fulfilled,
    }
  }

  l.WithFields(logrus.Fields{
    "num": len(requests),
  }).Debug("retrieved reviewed requests")

  return &proto.RequestList{
  	Requests: requests,
  }, nil
}


func (s *Server) GetLastRequests(ctx context.Context, req *proto.GetLastRequestsRequest) (*proto.RequestList, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  requestsPg, err := s.queries.GetRequestsReviewedLast(ctx, int32(req.Limit))

  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  requests := make([]*proto.Request, len(requestsPg))
  var carNumber *string
  var user *proto.User
  var pass *string
  for i, rrr := range requestsPg {
    if rrr.CarNumber.Valid {
      s := rrr.CarNumber.String
      carNumber = &s
    } else {
      carNumber = nil
    }
    if rrr.UserID.Valid {
      var updated *string
      if rrr.UserUpdatedAt.Valid {
        s := rrr.UserUpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
        updated = &s
      }
      user = &proto.User{
      	Id:               rrr.UserID.Int64,
      	Username:         &rrr.UserUsername.String,
      	Telegramname:     rrr.UserTelegramname.String,
      	Firstname:        rrr.UserFirstname.String,
      	Lastname:         &rrr.UserLastname.String,
      	Phone:            rrr.UserPhone.String,
      	Address:          rrr.UserAddress.String,
      	CarNumbers:       rrr.UserCarNumbers,
      	AddressConfirmed: rrr.UserAddressConfirmed.Bool,
      	Identified:       rrr.UserIdentified.Bool,
      	Activated:        rrr.UserActivated.Bool,
      	Lang:             rrr.UserLang.String,
        CreatedAt:        rrr.UserCreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
      	UpdatedAt:        updated,
      }
    } else {
      user = nil
    }
    if rrr.Pass.Status == pgtype.Present {
      pss := string(rrr.Pass.Bytes)
      pass = &pss
    } else {
      pass = nil
    }

    var reviewed *string
    if rrr.ReviewedAt.Valid {
      s := rrr.ReviewedAt.Time.Format("2006-01-02T15:04:05.999Z")
      reviewed = &s
    }
    var fulfilled *string
    if rrr.FulfilledAt.Valid {
      s := rrr.FulfilledAt.Time.Format("2006-01-02T15:04:05.999Z")
      fulfilled = &s
    }
    created := rrr.CreatedAt.Format("2006-01-02T15:04:05.999Z")
    requests[i] = &proto.Request{
    	Id:          rrr.ID,
    	Body:        rrr.Body,
    	Address:     rrr.Address.String,
    	CarNumber:   carNumber,
    	User:        user,
    	Status:      rrr.Status,
    	Pass:        pass,
    	CreatedAt:   &created,
    	ReviewedAt:   reviewed,
    	FulfilledAt: fulfilled,
    }
  }

  l.WithFields(logrus.Fields{
    "num": len(requests),
  }).Debug("retrieved last reviewed requests")

  return &proto.RequestList{
  	Requests: requests,
  }, nil
}

func (s *Server) GetRequest(ctx context.Context, req *proto.GetRequestRequest) (*proto.Request, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  requestPg, err := s.queries.GetRequestById(ctx, req.GetId())

  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  var carNumber *string
  var user *proto.User
  var pass *string
  if requestPg.CarNumber.Valid {
    s := requestPg.CarNumber.String
    carNumber = &s
  } else {
    carNumber = nil
  }
  if requestPg.UserID.Valid {
    var updated *string
    if requestPg.UserUpdatedAt.Valid {
      s := requestPg.UserUpdatedAt.Time.Format("2006-01-02T15:04:05.999Z")
      updated = &s
    }
    user = &proto.User{
      Id:               requestPg.UserID.Int64,
      Username:         &requestPg.UserUsername.String,
      Telegramname:     requestPg.UserTelegramname.String,
      Firstname:        requestPg.UserFirstname.String,
      Lastname:         &requestPg.UserLastname.String,
      Phone:            requestPg.UserPhone.String,
      Address:          requestPg.UserAddress.String,
      CarNumbers:       requestPg.UserCarNumbers,
      AddressConfirmed: requestPg.UserAddressConfirmed.Bool,
      Identified:       requestPg.UserIdentified.Bool,
      Activated:        requestPg.UserActivated.Bool,
      Lang:             requestPg.UserLang.String,
      CreatedAt:        requestPg.UserCreatedAt.Time.Format("2006-01-02T15:04:05.999Z"),
      UpdatedAt:        updated,
    }
  } else {
    user = nil
  }
  if requestPg.Pass.Status == pgtype.Present {
    pss := string(requestPg.Pass.Bytes)
    pass = &pss
  } else {
    pass = nil
  }

  var reviewed *string
  if requestPg.ReviewedAt.Valid {
    s := requestPg.ReviewedAt.Time.Format("2006-01-02T15:04:05.999Z")
    reviewed = &s
  }
  var fulfilled *string
  if requestPg.FulfilledAt.Valid {
    s := requestPg.FulfilledAt.Time.Format("2006-01-02T15:04:05.999Z")
    fulfilled = &s
  }
  created := requestPg.CreatedAt.Format("2006-01-02T15:04:05.999Z")
  request := &proto.Request{
    Id:          requestPg.ID,
    Body:        requestPg.Body,
    Address:     requestPg.Address.String,
    CarNumber:   carNumber,
    User:        user,
    Status:      requestPg.Status,
    Pass:        pass,
    CreatedAt:   &created,
    ReviewedAt:   reviewed,
    FulfilledAt: fulfilled,
  }

  l.Debug("returning request")

  return request, nil
}

func (s *Server) UpdateRequestStatus(ctx context.Context, req *proto.UpdateRequestStatusRequest) (*proto.UpdateRequestStatusResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  requestId, err := s.queries.UpdateRequestStatus(ctx, pg.UpdateRequestStatusParams{
  	ID:          req.Id,
  	Status:      sql.NullInt32{
  		Int32: req.GetStatus(),
  		Valid: req.Status != nil,
  	},
  	ReviewedAt:  sql.NullTime{
  		Time:  time.Now(),
  		Valid: req.SetReviewedAt != nil,
  	},
  	FulfilledAt: sql.NullTime{
  		Time:  time.Now(),
  		Valid: req.SetFulfilledAt != nil,
  	},
  })

  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  l.Debug("returning retrieved request")

  return &proto.UpdateRequestStatusResponse{
  	Id: requestId,
  }, nil
}

func (s *Server) UpdateRequestPass(ctx context.Context, req *proto.UpdateRequestPassRequest) (*proto.UpdateRequestPassResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  requestId, err := s.queries.UpdateRequestPass(ctx, pg.UpdateRequestPassParams{
  	ID:   req.Id,
  	Pass: pgtype.JSONB{
  		Bytes:  []byte(req.Pass),
  		Status: pgtype.Present,
  	},
  })

  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  l.Debug("returning updated request")

  return &proto.UpdateRequestPassResponse{
  	Id: requestId,
  }, nil
}

func (s *Server) DeleteRequest(ctx context.Context, req *proto.DeleteRequestRequest) (*proto.Request, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  requestPg, err := s.queries.DeleteRequest(ctx, req.Id)

  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  var carNumber *string
  var user *proto.User
  var pass *string
  if requestPg.CarNumber.Valid {
    s := requestPg.CarNumber.String
    carNumber = &s
  } else {
    carNumber = nil
  }
  if requestPg.UserID.Valid {
    user = &proto.User{
      Id: requestPg.UserID.Int64,
    }
  } else {
    user = nil
  }
  if requestPg.Pass.Status == pgtype.Present {
    pss := string(requestPg.Pass.Bytes)
    pass = &pss
  } else {
    pass = nil
  }

  var reviewed *string
  if requestPg.ReviewedAt.Valid {
    s := requestPg.ReviewedAt.Time.Format("2006-01-02T15:04:05.999Z")
    reviewed = &s
  }
  var fulfilled *string
  if requestPg.FulfilledAt.Valid {
    s := requestPg.FulfilledAt.Time.Format("2006-01-02T15:04:05.999Z")
    fulfilled = &s
  }
  created := requestPg.CreatedAt.Format("2006-01-02T15:04:05.999Z")
  request := &proto.Request{
    Id:          requestPg.ID,
    Body:        requestPg.Body,
    Address:     requestPg.UserAddress.String,
    CarNumber:   carNumber,
    User:        user,
    Status:      requestPg.Status,
    Pass:        pass,
    CreatedAt:   &created,
    ReviewedAt:   reviewed,
    FulfilledAt: fulfilled,
  }

  l.Debug("deleted request")

  return request, nil
}
