package section

import (
	"context"
	"database/sql"

	"github.com/jackc/pgtype"
	"github.com/sirupsen/logrus"
	"gitlab.com/m11259/object-model-service/internal/pg"
	"gitlab.com/m11259/object-model-service/proto/gen/oms/v1"
	"gitlab.com/m11259/object-model-service/internal/utils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	proto.UnimplementedSectionServiceServer
	queries *pg.Queries
	log     *logrus.Entry
}

func New(queries *pg.Queries, l *logrus.Entry) *Server {
	return &Server{
		queries: queries,
		log:     l,
	}
}

func (s *Server) CreateSection(ctx context.Context, req *proto.CreateSectionRequest) (*proto.Section, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	data := pgtype.JSONB{}
	data.Set(req.Data)

	section, err := s.queries.CreateSection(ctx, pg.CreateSectionParams{
		Name: req.GetName(),
		Data: data,
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.WithField("id", section.ID).Debug(`Section created`)

	return &proto.Section{
		Id:   section.ID,
		Name: section.Name,
		Data: string(section.Data.Bytes),
	}, nil
}

func (s *Server) GetSections(ctx context.Context, req *proto.GetSectionsRequest) (*proto.SectionList, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	sections, err := s.queries.GetAllSections(ctx)

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

	protoSections := make([]*proto.Section, len(sections))

	for i, s2 := range sections {
		protoSections[i] = &proto.Section{
			Id:   s2.ID,
			Name: s2.Name,
			Data: string(s2.Data.Bytes),
		}
	}

	return &proto.SectionList{
		Sections: protoSections,
	}, nil
}

func (s *Server) UpdateSection(ctx context.Context, req *proto.UpdateSectionRequest) (*proto.Section, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	var sectionDataStatus pgtype.Status = pgtype.Null

	sectionData := []byte{}

	if req.GetData() != "" {
		sectionDataStatus = pgtype.Present
		sectionData = []byte(req.GetData())
	}

	section, err := s.queries.UpdateSection(ctx, pg.UpdateSectionParams{
		Name: sql.NullString{
			String: req.GetName(),
			Valid:  req.Name != nil,
		},
		Data: pgtype.JSONB{
			Bytes:  sectionData,
			Status: sectionDataStatus,
		},
		ID: sql.NullInt64{
			Int64: req.GetId(),
			Valid: true,
		},
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Debug(`Section updated`)

	return &proto.Section{
		Id:   section.ID,
		Name: section.Name,
		Data: string(section.Data.Bytes),
	}, nil
}

func (s *Server) DeleteSection(ctx context.Context, req *proto.DeleteSectionRequest) (*proto.Section, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	section, err := s.queries.DeleteSection(ctx, req.GetId())

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Debug(`Section deleted`)

	return &proto.Section{
		Id:   section.ID,
		Name: section.Name,
		Data: string(section.Data.Bytes),
	}, nil
}
