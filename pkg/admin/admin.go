package admin

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
	"gitlab.com/m11259/object-model-service/internal/pg"
	"gitlab.com/m11259/object-model-service/proto/gen/oms/v1"
	"gitlab.com/m11259/object-model-service/internal/utils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	proto.UnimplementedAdminServiceServer
	queries *pg.Queries
	log     *logrus.Entry
}

func New(queries *pg.Queries, l *logrus.Entry) *Server {
	return &Server{
		queries: queries,
		log:     l,
	}
}

func (s *Server) CreateAdmin(
	ctx context.Context,
	req *proto.CreateAdminRequest,
) (*proto.Admin, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  if req.Username == "" || req.Password == "" {
    return nil, status.Error(codes.InvalidArgument, "empty username or password")
  }

  admin, err := s.queries.CreateAdmin(ctx, pg.CreateAdminParams{
		Username: req.Username,
		Password: req.Password,
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    if rpcStatusError.Error() == "no result" {
      return nil, nil
    }
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Info("created admin")

	return &proto.Admin{
		Id:       admin.ID,
		Username: admin.Username,
	}, nil
}

func (s *Server) ListAdmins(ctx context.Context, req *proto.ListAdminsRequest) (*proto.ListAdminsResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  adminsPg, err := s.queries.GetAllAdmins(ctx)

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Info("retrieved all admins")

  admins := make([]*proto.Admin, len(adminsPg))

  for i, a := range adminsPg {
    admins[i] = &proto.Admin{
    	Id:        a.ID,
    	Username:  a.Username,
    	Password:  a.Password,
    	CreatedAt: a.CreatedAt.Format("2006-01-02T15:04:05.999Z"),
    	UpdatedAt: a.UpdatedAt.Format("2006-01-02T15:04:05.999Z"),
    }
  }

	return &proto.ListAdminsResponse{
		Admins: admins,
	}, nil
}

func (s *Server) UpdateAdmin(ctx context.Context, req *proto.UpdateAdminRequest) (*proto.Admin, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  if req.GetUsername() == "" && req.GetPassword() == "" {
    return nil, status.Error(codes.InvalidArgument,
      "empty username and password. provide at least one of them.")
  }

  admin, err := s.queries.UpdateAdmin(ctx, pg.UpdateAdminParams{
  	Username: sql.NullString{
  		String: req.GetUsername(),
  		Valid:  req.Username != nil,
  	},
  	Password: sql.NullString{
  		String: req.GetPassword(),
  		Valid:  req.Password != nil,
  	},
  	ID:       req.Id,
  })

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Info("updated admin")

	return &proto.Admin{
		Id:        admin.ID,
		Username:  admin.Username,
		Password:  admin.Password,
    CreatedAt: admin.CreatedAt.Format("2006-01-02T15:04:05.999Z"),
    UpdatedAt: admin.UpdatedAt.Format("2006-01-02T15:04:05.999Z"),
	}, nil
}

func (s *Server) DeleteAdmin(ctx context.Context, req *proto.DeleteAdminRequest) (*proto.Admin, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  admin, err := s.queries.DeleteAdmin(ctx, req.GetId())

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Info("deleted admin")

	return &proto.Admin{
		Id:        admin.ID,
		Username:  admin.Username,
		Password:  admin.Password,
    CreatedAt: admin.CreatedAt.Format("2006-01-02T15:04:05.999Z"),
    UpdatedAt: admin.UpdatedAt.Format("2006-01-02T15:04:05.999Z"),
	}, nil
}
