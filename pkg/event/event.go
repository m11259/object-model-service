package event

import (
	"context"
	"errors"
	"io"

	"github.com/sirupsen/logrus"
	"gitlab.com/m11259/object-model-service/internal/broker"
	"gitlab.com/m11259/object-model-service/proto/gen/oms/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type EventServer struct {
	proto.UnimplementedEventServiceServer

	b broker.Broker
	l *logrus.Entry
}

func NewEventServer(b broker.Broker, log *logrus.Entry) *EventServer {
	return &EventServer{
		b: b,
		l: log,
	}
}

func (esrv *EventServer) CreateClient(
	ctx context.Context,
	req *proto.CreateClientRequest,
) (*proto.CreateClientResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	if client := esrv.b.GetClient(req.GetName()); client != nil {
		l.WithFields(logrus.Fields{
			"client": client.Name(),
		}).Info("Client already exists. Returning it")

		return &proto.CreateClientResponse{
			Name:   client.Name(),
			Status: "exists",
		}, nil
	}

	client := esrv.b.AddClient(req.GetName())
	if client == nil {
		return &proto.CreateClientResponse{}, errors.New("client not created")
	}

	l.WithFields(logrus.Fields{
		"client": client.Name(),
	}).Info("Created client")

	return &proto.CreateClientResponse{
		Name:   client.Name(),
		Status: "created",
	}, nil
}

func (esrv *EventServer) Subscribe(src proto.EventService_SubscribeServer) error {
	var brokerClient *broker.Client

	msg, err := src.Recv()
	if err == io.EOF {
		esrv.l.WithFields(logrus.Fields{
			"client": "<unknown>",
		}).Info("Client has closed connection")

		return err
	}

	if err != nil {
		esrv.l.WithError(err).WithFields(logrus.Fields{
			"client": "<unknown>",
		}).Info("Unable to read from client")

		return err
	}

	brokerClient = esrv.b.GetClient(msg.GetClientName())
	if brokerClient == nil {
		esrv.l.WithFields(logrus.Fields{
			"client": msg.GetClientName(),
		}).Info("Created client")

		brokerClient = esrv.b.AddClient(msg.GetClientName())
	}

	topics := msg.GetTopic()

	brokerClient.UnsubscribeAll()
	brokerClient.Subscribe(topics...)

  esrv.l.WithFields(logrus.Fields{
    "client": brokerClient.Name(),
    "topics": topics,
  }).Info(`Client subscribed to topics`)

	change := make(chan string)
	// start goroutine for getting incoming subscribe changes
	go func() {
		for {
			msg, err := src.Recv()
			if err == io.EOF {
				esrv.l.WithFields(logrus.Fields{
					"client": brokerClient.Name(),
				}).Info("Client has closed connection")
				change <- "close"

				return
			}

			if err != nil {
				esrv.l.WithError(err).WithFields(logrus.Fields{
					"client": brokerClient.Name(),
				}).Info("Unable to read from client")
				change <- "close"

				return
			}

			brokerClient = esrv.b.GetClient(msg.GetClientName())
			if brokerClient == nil {
				esrv.l.WithFields(logrus.Fields{
					"client": brokerClient.Name(),
				}).Info("Created client")

				brokerClient = esrv.b.AddClient(msg.GetClientName())
			}

			topics := msg.GetTopic()

			brokerClient.UnsubscribeAll()
			brokerClient.Subscribe(topics...)
			change <- "client change"

			esrv.l.WithFields(logrus.Fields{
				"client": brokerClient.Name(),
				"topics": topics,
			}).Info("Client changed topics")
		}
	}()

	events := brokerClient.Buffer().ReadChannel()

	for {
		select {
		case c := <-change:
			switch c {
			case "close":
				return nil
			case "client change":
				events = brokerClient.Buffer().ReadChannel()
			}
		// push new incoming events
		case event := <-events:
			esrv.l.WithFields(logrus.Fields{
				"topic":  event.Topic,
				"client": brokerClient.Name(),
			}).Debug("Sending data topic to client")
			src.Send(&proto.SubscribeResponse{
				Topic: event.Topic,
				Data:  string(event.Data),
			})
		}
	}
}

func (esrv *EventServer) Push(ctx context.Context, req *proto.PushRequest) (*proto.PushResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	l.WithFields(logrus.Fields{
		"topic": req.GetTopic(),
		"data":  req.GetData(),
	}).WithContext(ctx).Debug("Received event")

	esrv.b.Emit(req.GetTopic(), []byte(req.GetData()))

	return &proto.PushResponse{
		Topic: req.GetTopic(),
	}, nil
}
