package address

import (
	"context"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/m11259/object-model-service/internal/utils"
	"gitlab.com/m11259/object-model-service/pkg/repository"
	proto "gitlab.com/m11259/object-model-service/proto/gen/oms/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
  proto.UnimplementedAddressServiceServer
	log     *logrus.Entry
  repository *repository.Repository
}


func (s *Server) ListAddresses(ctx context.Context, req *proto.ListAddressesRequest) (*proto.ListAddressesResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  addresses, err := s.repository.ListAddresses(ctx)
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  return &proto.ListAddressesResponse{
  	Addresses: addresses,
  }, nil
}

func (s *Server) AddressExists(ctx context.Context, req *proto.AddressExistsRequest) (*proto.AddressExistsResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

  exists, err := s.repository.ExistsAddress(ctx, req.GetAddress())
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }

  return &proto.AddressExistsResponse{
  	Exists: exists,
  }, nil
}

func (s *Server) AddAddress(ctx context.Context, req *proto.AddAddressRequest) (*proto.AddAddressResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  
  err := s.repository.AddAddress(ctx, req.GetAddress()...)
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }
  return &proto.AddAddressResponse{}, nil
}

func (s *Server) RemoveAddress(ctx context.Context, req *proto.RemoveAddressRequest) (*proto.RemoveAddressResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  
  err := s.repository.RemoveAddress(ctx, req.GetAddress()...)
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }
  return &proto.RemoveAddressResponse{}, nil
}

func (s *Server) GetAddressPaymentQuota(ctx context.Context, req *proto.GetAddressPaymentQuotaRequest) (*proto.GetAddressPaymentQuotaResponse, error) {
  // l, ok := ctx.Value("log").(*logrus.Entry)
  // if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  year, month := req.GetYear(), req.GetMonth()
  results := make([]float32, len(req.GetAddress()))
  for i, v := range req.GetAddress() {
    quota, _ := s.repository.GetPaymentQuota(ctx, int(year), int(month), v)
    results[i] = float32(quota)
  }
  return &proto.GetAddressPaymentQuotaResponse{
  	Quotas: results,
  	Year:   year,
  	Month:  month,
  }, nil
}

func (s *Server) SetAddressPaymentQuota(ctx context.Context, req *proto.SetAddressPaymentQuotaRequest) (*proto.SetAddressPaymentQuotaResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  err := s.repository.SetPaymentQuota(ctx, int(req.GetYear()), int(req.GetMonth()), req.GetAddress(), float64(req.GetQuota()))
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }
  return &proto.SetAddressPaymentQuotaResponse{}, nil
}

func (s *Server) GetDefaultPaymentQuota(ctx context.Context, req *proto.GetDefaultPaymentQuotaRequest) (*proto.GetDefaultPaymentQuotaResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  quota, err := s.repository.GetPaymentQuotaDefault(ctx, int(req.GetYear()), int(req.GetMonth()))
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }
  return &proto.GetDefaultPaymentQuotaResponse{
  	Quota: float32(quota),
  	Year:  req.GetYear(),
  	Month: req.GetMonth(),
  }, nil
}

func (s *Server) SetDefaultPaymentQuota(ctx context.Context, req *proto.SetDefaultPaymentQuotaRequest) (*proto.SetDefaultPaymentQuotaResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  err := s.repository.SetPaymentQuotaDefault(ctx, int(req.GetYear()), int(req.GetMonth()), float64(req.GetQuota()))
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }
  return &proto.SetDefaultPaymentQuotaResponse{}, nil
}

func (s *Server) GetAddressPaymentsStats(ctx context.Context, req *proto.GetAddressPaymentsStatsRequest) (*proto.GetAddressPaymentsStatsResponse, error) {
  // l, ok := ctx.Value("log").(*logrus.Entry)
  // if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  year, month := req.GetYear(), req.GetMonth()
  results := make([]*proto.GetAddressPaymentsStatsResponse_AddressStats, 0, 1)
  var status bool
  var amount float64
  for _, v := range req.GetAddress() {
    status, _ = s.repository.GetPaymentStatus(ctx, int(year), int(month), v)
    amountAddress := s.repository.GetPaymentStats(ctx, int(year), int(month), v)
    if amountAddress == nil || len(amountAddress) == 0 {
      amount = 0
    } else {
      amount = amountAddress[0].Value
    }
    results = append(results, &proto.GetAddressPaymentsStatsResponse_AddressStats{
    	Address: v,
    	Paid:    status,
    	Amount:  float32(amount),
    })
  }
  return &proto.GetAddressPaymentsStatsResponse{
  	Results: results,
  	Year:    year,
  	Month:   month,
  }, nil
}

func (s *Server) SetAddressPaymentStatus(ctx context.Context, req *proto.SetAddressPaymentStatusRequest) (*proto.SetAddressPaymentStatusResponse, error) {
  // l, ok := ctx.Value("log").(*logrus.Entry)
  // if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  year, month := req.GetYear(), req.GetMonth()
  s.repository.SetPaymentStatus(ctx, int(year), int(month), req.GetAddress(), req.GetPaid())
  return &proto.SetAddressPaymentStatusResponse{}, nil
}

func (s *Server) SetAddressPayment(ctx context.Context, req *proto.SetAddressPaymentRequest) (*proto.SetAddressPaymentResponse, error) {
  // l, ok := ctx.Value("log").(*logrus.Entry)
  // if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  year, month := req.GetYear(), req.GetMonth()
  s.repository.SetPaymentStats(ctx, int(year), int(month), req.GetAddress(), float64(req.GetAmount()))
  return &proto.SetAddressPaymentResponse{}, nil
}

func (s *Server) DisableAddress(ctx context.Context, req *proto.DisableAddressRequest) (*proto.DisableAddressResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  err := s.repository.SetAddressDisabled(ctx, req.GetAddress())
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }
  return &proto.DisableAddressResponse{}, nil
}

func (s *Server) EnableAddress(ctx context.Context, req *proto.EnableAddressRequest) (*proto.EnableAddressResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  err := s.repository.SetAddressEnabled(ctx, req.GetAddress())
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }
  return &proto.EnableAddressResponse{}, nil
}

func (s *Server) SetAddressMetadata(ctx context.Context, req *proto.SetAddressMetadataRequest) (*proto.SetAddressmetadataResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  value := strings.Join(req.GetValue(), ";")
  err := s.repository.SetAddressMetadata(ctx, req.GetAddress(), req.GetKey(), value)
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }
  return &proto.SetAddressmetadataResponse{}, nil
}

func (s *Server) GetAddressMetadata(ctx context.Context, req *proto.GetAddressMetadataRequest) (*proto.GetAddressMetadataResponse, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }
  metas, err := s.repository.GetAddressMetadata(ctx, req.GetAddress())
  if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
  }
  if metas == nil { return nil, status.Error(codes.Unknown, "internal errro") }
  response := make(map[string]*proto.GetAddressMetadataResponse_MetaValues)
  for k, v := range metas {
    response[k] = &proto.GetAddressMetadataResponse_MetaValues{
    	Values: strings.Split(v, ";"),
    }
  }
  return &proto.GetAddressMetadataResponse{
  	Metas: map[string]*proto.GetAddressMetadataResponse_MetaValues{},
  }, nil
}
