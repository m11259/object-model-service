package guard

import (
	"context"
	"database/sql"

	"github.com/jackc/pgtype"
	"github.com/sirupsen/logrus"
	"gitlab.com/m11259/object-model-service/internal/pg"
	"gitlab.com/m11259/object-model-service/internal/utils"
	"gitlab.com/m11259/object-model-service/proto/gen/oms/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	proto.UnimplementedGuardServiceServer
	queries *pg.Queries
	log     *logrus.Entry
}

func New(queries *pg.Queries, l *logrus.Entry) *Server {
	return &Server{
		queries: queries,
		log:     l,
	}
}

func (s *Server) CreateGuard(ctx context.Context, req *proto.CreateGuardRequest) (*proto.Guard, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	if req.Username == "" {
		return nil, status.Error(codes.InvalidArgument, "empty username")
	}

	if req.Password == "" {
		return nil, status.Error(codes.InvalidArgument, "empty password")
	}

	guard, err := s.queries.CreateGuard(ctx, pg.CreateGuardParams{
		Username: req.GetUsername(),
		Password: req.GetPassword(),
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Debug("Created guard")

	return &proto.Guard{
		Id:        guard.ID,
		Username:  guard.Username,
		Section:   nil,
		CreatedAt: guard.CreatedAt.Format("2006-01-02T15:04:05.999Z"),
	}, nil
}

func (s *Server) GetAllGuards(ctx context.Context, req *proto.GetAllGuardsRequest) (*proto.GuardList, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	guards, err := s.queries.GetGuardsList(ctx)
	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

	grpcGuards := make([]*proto.Guard, len(guards))
  var section *proto.Section
	for i, gglr := range guards {
    if gglr.SectionID.Valid {
      section = &proto.Section{
				Id:   gglr.SectionID.Int64,
				Name: gglr.SectionName.String,
				Data: string(gglr.SectionData.Bytes),
			}
    } else {
      section = nil
    }

		grpcGuards[i] = &proto.Guard{
			Id:       int64(gglr.ID),
			Username: gglr.Username,
			Section: section,
			CreatedAt: gglr.CreatedAt.Format("2006-01-02T15:04:05.999Z"),
		}
	}

  l.WithField("num", len(grpcGuards)).Debug("returning retrieved guards")

	return &proto.GuardList{
		List: grpcGuards,
	}, nil
}

func (s *Server) GetGuard(ctx context.Context, req *proto.GetGuardRequest) (*proto.Guard, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	if req.GetId() == 0 && req.GetUsername() == "" {
    l.Print("Invalid request. Empty request paramethers")
    return nil, status.Error(codes.InvalidArgument, "empty request paramethers")
	}

	if req.GetId() != 0 && req.GetUsername() != "" {
    l.Print("Invalid request. Both paramethers are speified")
    return nil, status.Error(codes.InvalidArgument, "both paramethers are specified")
	}

	var guard *proto.Guard

	var err error

	if req.GetId() != 0 {
		guardById, errById := s.queries.GetGuardById(ctx, req.GetId())
		if errById != nil {
			err = errById
		}

    var sectionData string
    if guardById.SectionData.Status == pgtype.Present {
      sectionData = string(guardById.SectionData.Bytes)
    } else {
      sectionData = "{}"
    }

    var section *proto.Section
    if guardById.SectionID.Valid {
      section = &proto.Section{
				Id:   guardById.SectionID.Int64,
				Name: guardById.SectionName.String,
				Data: sectionData,
			}
    } else {
      section = nil
    }

		guard = &proto.Guard{
			Id:       guardById.ID,
			Username: guardById.Username,
			Section: section,
			CreatedAt: guardById.CreatedAt.Format("2006-01-02T15:04:05.999Z"),
		}
	} else {
		guardByUsername, errByUsername := s.queries.GetGuardByUsername(ctx, req.GetUsername())
		if errByUsername != nil {
			err = errByUsername
		}

    var sectionData string
    if guardByUsername.SectionData.Status == pgtype.Present {
      sectionData = string(guardByUsername.SectionData.Bytes)
    } else {
      sectionData = "{}"
    }

    var section *proto.Section
    if guardByUsername.SectionID.Valid {
      section = &proto.Section{
				Id:   guardByUsername.SectionID.Int64,
				Name: guardByUsername.SectionName.String,
				Data: sectionData,
			}
    } else {
      section = nil
    }

		guard = &proto.Guard{
			Id:       guardByUsername.ID,
			Username: guardByUsername.Username,
			Section: section,
			CreatedAt: guardByUsername.CreatedAt.Format("2006-01-02T15:04:05.999Z"),
		}
	}

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

	return guard, nil
}

func (s *Server) UpdateGuard(ctx context.Context, req *proto.UpdateGuardRequest) (*proto.Guard, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	guard, err := s.queries.UpdateGuard(ctx, pg.UpdateGuardParams{
		ID: req.GetId(),
		Username: sql.NullString{
			String: req.GetUsername(),
			Valid:  req.GetUsername() != "",
		},
		Password: sql.NullString{
			String: req.GetPassword(),
			Valid:  req.GetPassword() != "",
		},
		SectionID: sql.NullInt64{
			Int64: req.GetSectionId(),
			Valid: req.GetSectionId() != 0,
		},
	})

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Debug(`Guard record updated`)

	return &proto.Guard{
		Id:       guard.ID,
		Username: guard.Username,
		Section: &proto.Section{
			Id:   guard.SectionID,
			Name: guard.SectionName,
			Data: string(guard.SectionData.Bytes),
		},
    CreatedAt: guard.CreatedAt.Format("2006-01-02T15:04:05.999Z"),
	}, nil
}

func (s *Server) DeleteGuard(ctx context.Context, req *proto.DeleteGuardRequest) (*proto.Guard, error) {
  l, ok := ctx.Value("log").(*logrus.Entry)
  if !ok { return nil, status.Error(codes.DataLoss, "internal error") }

	guard, err := s.queries.DeleteGuard(ctx, req.GetId())

	if err != nil {
    rpcStatusError := utils.ConvertPGXToStatusError(err)
    l.WithError(err).Error(rpcStatusError.Error())
		return nil, rpcStatusError
	}

  l.Debug(`Successfully deleted guard`)

	return &proto.Guard{
		Id:        guard.ID,
		Username:  guard.Username,
		Section:   &proto.Section{},
    CreatedAt: guard.CreatedAt.Format("2006-01-02T15:04:05.999Z"),
	}, nil
}
