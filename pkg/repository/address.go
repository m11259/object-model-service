package repository

import "context"

const ADDRESSES_SET = "addresses"

func (r *Repository) ListAddresses(ctx context.Context) ([]string, error) {
  addresses, err := r.redis.SMembers(ctx, ADDRESSES_SET).Result()
  if err != nil { return nil, err }
  return addresses, nil
}

func (r *Repository) ExistsAddress(ctx context.Context, address string) (bool, error) {
  exists, err := r.redis.SIsMember(ctx, ADDRESSES_SET, address).Result()
  if err != nil { return false, err }
  return exists, nil
}

func (r *Repository) AddAddress(ctx context.Context, address ...string) error {
  s := make([]interface{}, len(address))
  for i, v := range address { s[i] = v }
  err := r.redis.SAdd(ctx, ADDRESSES_SET, s...).Err()
  if err != nil { return err }
  return nil
}

func (r *Repository) RemoveAddress(ctx context.Context, address ...string) error {
  s := make([]interface{}, len(address))
  for i, v := range address { s[i] = v }
  err := r.redis.SRem(ctx, ADDRESSES_SET, s...).Err()
  if err != nil { return err }
  return nil
}

func (r *Repository) keyAddressMeta(address string) string {
  return "address:metadata:" + address
}

func (r *Repository) SetAddressMetadata(ctx context.Context, address string,
name string, value any) error {
  key := r.keyAddressMeta(address)
  _, err := r.redis.HSet(ctx, key, name, value).Result()
  if err != nil { return err }
  return nil
}

func (r *Repository) GetAddressMetadata(ctx context.Context, address string) (map[string]string, error) {
  key := r.keyAddressMeta(address)
  values, err := r.redis.HGetAll(ctx, key).Result()
  if err != nil { return nil, err }
  return values, nil
}

func (r *Repository) keyAddressDisabled() string {
  return "addresses:disabled"
}

func (r *Repository) SetAddressDisabled(ctx context.Context, address string) error {
  key := r.keyAddressDisabled()
  _, err := r.redis.SAdd(ctx, key, address).Result()
  if err != nil { return err }
  return nil
}

func (r *Repository) IsAddressDisabled(ctx context.Context, address string) (bool, error) {
  key := r.keyAddressDisabled()
  res, err := r.redis.SIsMember(ctx, key, address).Result()
  if err != nil { return false, err }
  return res, nil
}

func (r *Repository) SetAddressEnabled(ctx context.Context, address string) error {
  key := r.keyAddressDisabled()
  _, err := r.redis.SRem(ctx, key, address).Result()
  if err != nil { return err }
  return nil
}
