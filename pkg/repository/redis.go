package repository

import (
	"os"
	"strconv"

	"github.com/go-redis/redis/v8"
)

type Repository struct {
  redis *redis.Client
}

func New() *Repository {
  redisHost := os.Getenv("REDIS_HOST")
  if redisHost == "" { panic("REDIS_HOST not valid") }

  redisPort := os.Getenv("REDIS_PORT")
  if redisPort == "" { panic("REDIS_PORT not valid") }

  redisAddr := ""

  redisDBStr := os.Getenv("REDIS_DB")
  if redisDBStr == "" { panic("REDIS_DB not valid") }

  redisDB, err := strconv.ParseUint(redisDBStr, 10, 64)
  if err != nil { panic("REDIS_DB not valid") }

  redisClient := redis.NewClient(&redis.Options{
    Addr: redisAddr,
    DB: int(redisDB),
  })

  rep := &Repository{
    redis: redisClient,
  }
  return rep
}
