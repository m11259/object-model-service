package repository

import (
	"context"
	"errors"
	"fmt"
	"strconv"
)

func (r *Repository) keyPaymentStats(year int, month int) string {
  return fmt.Sprintf("payments:stats:%d:%d", year, month)
}

func (r *Repository) SetPaymentStats(ctx context.Context, year, month int, address string, value float64) {
  key := r.keyPaymentStats(year, month)
  _, err := r.redis.HSet(ctx, key, address, value).Result()
  if err != nil {
    // TODO: log error
    return
  }
}

func (r *Repository) GetPaymentStats(ctx context.Context, year, month int, address ...string) []struct{ Address string; Value float64 } {
  key := r.keyPaymentStats(year, month)
  results := make([]struct{ address string; value float64 }, 0)
  if len(address) == 0 {
    res, err := r.redis.HGetAll(ctx, key).Result()
    if err != nil {
      // TODO: log error
      return nil
    }
    var val float64
    for k, v := range res {
      val, err = strconv.ParseFloat(v, 64)
      if err != nil {
        // TODO: log warning of invalid value in hashmap
        continue
      }
      results = append(results, struct{ address string; value float64 }{
      	address: k,
      	value:   val,
      })
    }
  } else {
    var val float64
    var valStr string
    var err error
    for _, v := range address {
      valStr, err = r.redis.HGet(ctx, key, v).Result()
      if err != nil {
        // TODO: log error
        continue
      }
      val, err = strconv.ParseFloat(valStr, 64)
      if err != nil {
        // TODO: log warning of invalid value in hashmap
        continue
      }
      results = append(results, struct{ address string; value float64 }{
      	address: v,
      	value:   val,
      })
    }
  }
  return results
}

func (r *Repository) keyPaymentStatus(year, month int, address string) string {
  return fmt.Sprintf("payments:status:%d:%d:%s", year, month, address)
}

func (r *Repository) SetPaymentStatus(ctx context.Context, year, month int, address string, paid bool) {
  key := r.keyPaymentStatus(year, month, address)
  var value string
  if paid { value = "1" } else { value = "0" }
  if r.redis.Set(ctx, key, value, 0).Err() != nil {
    // TODO: log this error
  }
}

func (r *Repository) GetPaymentStatus(ctx context.Context, year, month int, address string) (bool, error) {
  key := r.keyPaymentStatus(year, month, address)
  res, err := r.redis.Get(ctx, key).Result()
  if err != nil {
    // TODO: log error
    return false, err
  }
  status, err := strconv.ParseBool(res)
  if err != nil {
    // TODO: log error
    return false, err
  }
  return status, nil
}

func (r *Repository) keyPaymentQuota(year, month int) string {
  return fmt.Sprintf("payments:quota:%d:%d", year, month)
}

func (r *Repository) SetPaymentQuota(ctx context.Context, year, month int, address string, quota float64) error {
  key := r.keyPaymentQuota(year, month)
  if err := r.redis.HSet(ctx, key, address, quota).Err(); err != nil {
    // TODO: log error
    return err
  }
  return nil
}

func (r *Repository) GetPaymentQuota(ctx context.Context, year, month int, address string) (float64, error) {
  key := r.keyPaymentQuota(year, month)
  res, err := r.redis.HGet(ctx, key, address).Result()
  if err != nil {
    // TODO: log error
    def, err := r.GetPaymentQuotaDefault(ctx, year, month)
    return def, err
  }
  quota, err := strconv.ParseFloat(res, 64)
  if err != nil {
    return 0, errors.New("invalid value in key " + key + " " + address)
  }
  return quota, nil
}

func (r *Repository) keyPaymentQuotaDefault(year, month int) string {
  return fmt.Sprintf("payments:quota:default:%d:%d", year, month)
}

func (r *Repository) GetPaymentQuotaDefault(ctx context.Context, year, month int) (float64, error) {
  key := r.keyPaymentQuotaDefault(year, month)
  res, err := r.redis.Get(ctx, key).Result()
  if err != nil {
    return 0, err
  }
  quota, err := strconv.ParseFloat(res, 64)
  if err != nil {
    // TODO: log error
    return 0, errors.New("invalid quota set for " + string(year) + string(month))
  }
  return quota, nil
}

func (r *Repository) SetPaymentQuotaDefault(ctx context.Context, year, month int, quota float64) error {
  key := r.keyPaymentQuotaDefault(year, month)
  if err := r.redis.Set(ctx, key, quota, 0).Err(); err != nil {
    // TODO: log error
    return err
  }
  return nil
}

func (r *Repository) keyPaymentsDebt(year, month int, address string) string {
  return fmt.Sprintf("payments:debts:%d:%d:%s", year, month, address)
}

func (r *Repository) SetPaymentsDebt(ctx context.Context, year, month int, address string, debt float64) error {
  key := r.keyPaymentsDebt(year, month, address)
  if err := r.redis.Set(ctx, key, debt, 0).Err(); err != nil {
    return err
  }
  return nil
}

func (r *Repository) GetPaymentsDebt(ctx context.Context, year, month int, address string) (float64, error) {
  key := r.keyPaymentsDebt(year, month, address)
  res, err := r.redis.Get(ctx, key).Result()
  if err != nil {
    return 0, err
  }
  quota, err := strconv.ParseFloat(res, 64)
  if err != nil {
    return 0, errors.New("invalid debt in " + key)
  }
  return quota, nil
}
