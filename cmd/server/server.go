package main

import (
	"context"
	"fmt"
	"net"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/sirupsen/logrus"
	"gitlab.com/m11259/object-model-service/internal/log"
	"gitlab.com/m11259/object-model-service/internal/migrations"
	"gitlab.com/m11259/object-model-service/internal/pg"
	"google.golang.org/grpc"
  "google.golang.org/grpc/reflection"
)

func startGRPC(queries *pg.Queries, l *logrus.Logger) {
	port := os.Getenv("PORT")

  address := fmt.Sprintf("[::]:%s", port)

  l.Info(`Opening tcp on address `, address)

	lis, err := net.Listen("tcp", address)

	if err != nil {
		logrus.WithError(err).Fatal("Failed to start GRPC server")
	}

	gserver := grpc.NewServer(
    grpc.ChainUnaryInterceptor(
      CallerAuthUnaryServerInterceptor(l),
      TimingLoggerUnaryServerInterceptor,
    ),
  )

	// Register all Services
	attachService(gserver, queries, l)

  reflection.Register(gserver)

	sigCh := make(chan os.Signal, 1)

	signal.Notify(sigCh, os.Interrupt, syscall.SIGTERM)

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		s := <-sigCh
		logrus.WithField("signal", s).Info("Attempting graceful shutdown")
		gserver.GracefulStop()
		// grpc.Stop() // leads to error while receiving stream response:
		// rpc error: code = Unavailable desc = transport is closing
		wg.Done()
	}()

	logrus.Info("Starting GRPC server")

	if err := gserver.Serve(lis); err != nil {
		logrus.WithError(err).Error("Failed to start GRPC server")
	}

	wg.Wait()

	logrus.Info("Clean shutdown")
}

func connectToPostgres(l *logrus.Logger) *pgxpool.Pool {
	connString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable pool_max_conns=4",
		os.Getenv("POSTGRES_HOST"),
		os.Getenv("POSTGRES_PORT"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB"),
	)

	l.WithField("connection", connString).Trace()

	context, cancel := context.WithCancel(context.Background())
	defer cancel()

	pool, err := pgxpool.Connect(context, connString)
	if err != nil {
		l.WithError(err).Fatal("Failed to connect to Postgres Database")
	}
  return pool
}

func main() {
	l := log.New()


	pool := connectToPostgres(l)

  queries := pg.New(pool)

  pgConnection, err := pool.Acquire(context.Background())
  if err != nil {
    l.WithError(err).Error()
  }

  schemaVersion, err := strconv.ParseInt(os.Getenv("MIGRATE_VERSION"), 10, 64)
  if err != nil {
    l.WithError(err).Fatal(`database schema version to migrate to is not
    defined correctly via MIGRATE_VERSION`)
  }

	migrations.Migrate(l, pgConnection.Conn(), "/migrations", int(schemaVersion))

	startGRPC(queries, l)
}
