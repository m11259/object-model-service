package main

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/m11259/object-model-service/internal/broker"
	"gitlab.com/m11259/object-model-service/internal/pg"
	"gitlab.com/m11259/object-model-service/internal/utils"
	"gitlab.com/m11259/object-model-service/pkg/admin"
	"gitlab.com/m11259/object-model-service/pkg/event"
	"gitlab.com/m11259/object-model-service/pkg/guard"
	"gitlab.com/m11259/object-model-service/pkg/request"
	"gitlab.com/m11259/object-model-service/pkg/section"
	"gitlab.com/m11259/object-model-service/pkg/user"
	"gitlab.com/m11259/object-model-service/proto/gen/oms/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/reflect/protoreflect"
)

const GRPC_SERVICE_HEADER = "grpc-caller-service"

func attachService(gserver *grpc.Server, q *pg.Queries, l *logrus.Logger) {
  brk := broker.NewMemoryBroker()

  go brk.Start()

	proto.RegisterEventServiceServer(
		gserver,
		event.NewEventServer(
			brk,
			l.WithField("service", "event"),
		),
	)

	proto.RegisterAdminServiceServer(
		gserver,
		admin.New(q, l.WithField("service", "admin")),
	)

	proto.RegisterGuardServiceServer(
		gserver,
		guard.New(q, l.WithField("service", "guard")),
	)

	proto.RegisterRequestServiceServer(
		gserver,
		request.New(q, l.WithField("service", "request")),
	)

	proto.RegisterSectionServiceServer(
		gserver,
		section.New(q, l.WithField("service", "section")),
	)

	proto.RegisterUserServiceServer(
		gserver,
		user.New(q, l.WithField("service", "user")),
	)
}

func TimingLoggerUnaryServerInterceptor(
  ctx context.Context,
  req interface{},
  info *grpc.UnaryServerInfo,
  handler grpc.UnaryHandler,
) (resp interface{}, err error) {
  start := time.Now()
  resp, err = handler(ctx, req)
  duration := time.Now().Sub(start)
  logrus.WithFields(logrus.Fields{
    "handled_in": duration,
    "method": info.FullMethod,
    "err": err,
    "response_nil": resp == nil,
  }).Info()
  return
}

func CallerAuthUnaryServerInterceptor(l *logrus.Logger) grpc.UnaryServerInterceptor {
  return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
    md, ok := metadata.FromIncomingContext(ctx)
    if !ok {
      return nil, status.Error(codes.Unauthenticated, "provide " + GRPC_SERVICE_HEADER + " metadata")
    }
    caller := md.Get(GRPC_SERVICE_HEADER)
    if len(caller) == 0 {
      return nil, status.Error(codes.Unauthenticated, "provide " + GRPC_SERVICE_HEADER + " metadata")
    }

    reqProto, ok := req.(interface{ ProtoReflect() protoreflect.Message })
    if !ok {
      l.WithField("caller", caller).Warning("proto message can not be reflected")
      return nil, status.Error(codes.Unknown, "message can not be reflected")
    }
    lw := l.WithField("caller", caller).WithFields(*utils.RepresentProtoMesssageToLogrusFields(reqProto.ProtoReflect()))
    c := context.WithValue(ctx, "log", lw)
    return handler(c, req)
  }
}
