sqlc:
	./scripts/sqlc.sh

protoc:
	protoc --go_out=internal --go_opt=paths=source_relative --go-grpc_out=internal --go-grpc_opt=paths=source_relative proto/*

build:
	CGO_ENABLED=0 GOOS=linux go build -installsuffix cgo -o server ./cmd/server

install-go-lint:
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest

lint:
	golangci-lint run

new_migration:
	docker run -v $(PWD)/sql/migrations:/migrations --network host migrate/migrate create -dir=/migrations/ -ext sql -seq $(name)

.PHONY: build
