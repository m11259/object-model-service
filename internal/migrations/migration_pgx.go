package migrations

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"regexp"
	"sort"
	"strconv"

	"github.com/jackc/pgx/v4"
	"github.com/sirupsen/logrus"
)

type MigrationDirection string
const (
  MigrationUp MigrationDirection = "up"
  MigrationDown = "down"
)

type Migration struct {
  N         uint64
  Name      string
  Direction MigrationDirection
  Data      []byte
}

func (m Migration) String() string {
  return fmt.Sprintf("<Migration %d - %s -> %s\n%s>", m.N, m.Name, m.Direction, m.Data)
}

func Migrate(l *logrus.Logger, pg *pgx.Conn, migrationsPath string, version int) {
  migrations, err := readMigrations(migrationsPath)

  if err != nil {
    l.WithError(err).Fatal("Unable to read migrations")
  }

  err = createVersionsTable(pg)
  if err != nil {
    l.WithError(err).Fatal()
  }

  currentVersion, ok := getCurrentVersion(pg)

  l.WithFields(logrus.Fields{
    "currentVersion": currentVersion,
    "updateVersion": version,
    "dbVersionPresent": ok,
  }).Info(`Current Postgres schema version is `, currentVersion)

  diff := version - currentVersion

  if diff == 0 {
    l.Info("No migrations needed")
    return
  }

  order := buildUpgradeDowngradeMigrationsOrder(currentVersion, version, migrations)

  var ver int

  for _, m := range order {
    l.WithFields(logrus.Fields{
      "migrationName": m.Name,
      "migrationNum": m.N,
      "migrationDirection": m.Direction,
    }).Info("Applying migration...")
    err = applyMigration(pg, m)
    if err != nil {
      l.WithError(err).WithFields(logrus.Fields{
        "migrationName": m.Name,
        "migrationNum": m.N,
        "migrationDirection": m.Direction,
      }).Fatal(`failed applying migration`)
    }

    if diff < 0 {
      ver = int(m.N) - 1
    } else {
      ver = int(m.N)
    }

    err = setCurrentVersion(pg, ver)
    if err != nil {
      l.WithError(err).WithFields(logrus.Fields{
        "migrationName": m.Name,
        "migrationDirection": m.Direction,
        "versionToSet": ver,
      }).Fatal(`error saving version to database`)
    }
  }
}

func buildUpgradeDowngradeMigrationsOrder(fromVersion int, toVersion int, migrations []*Migration) []*Migration {
  var direction MigrationDirection
  if fromVersion < toVersion {
    direction = MigrationUp
  } else if toVersion < fromVersion {
    direction = MigrationDown
  } else {
    return []*Migration{}
  }

  migrationsToApply := make([]*Migration, 0)
  for _, m := range migrations {
    if direction == MigrationUp && m.Direction == direction && m.N > uint64(fromVersion) && m.N <= uint64(toVersion) {
      migrationsToApply = append(migrationsToApply, m)
    }
    if direction == MigrationDown && m.Direction == direction && m.N <= uint64(fromVersion) && m.N > uint64(toVersion) {
      migrationsToApply = append(migrationsToApply, m)
    }
  }

  sort.SliceStable(migrationsToApply, func(i, j int) bool {
    if direction == MigrationUp {
      return migrationsToApply[i].N < migrationsToApply[j].N
    }
    return migrationsToApply[i].N > migrationsToApply[j].N
  })

  return migrationsToApply
}

func isDirectory(path string) (bool, error) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false, err
	}

	return fileInfo.IsDir(), err
}

func readMigrations(migrationsPath string) ([]*Migration, error) {
  isDir, err := isDirectory(migrationsPath)
  if err != nil { return nil, err }

  if !isDir {
    return nil, errors.New("migration path is not a directory")
  }

  filesInfo, err := ioutil.ReadDir(migrationsPath)
  if err != nil { return nil, err }

  migrations := make([]*Migration, 0, len(filesInfo))

  migrationNameReg := regexp.MustCompile(`(\d+)_([a-z_]+)\.(up|down).sql`)

  var (
    num uint64
    t MigrationDirection
    contents []byte
  )

  for _, fi := range filesInfo {
    if !migrationNameReg.Match([]byte(fi.Name())) {
      continue
    }

    matches := migrationNameReg.FindStringSubmatch(fi.Name())

    if matches[3] == "up" {
      t = MigrationUp
    } else if matches[3] == "down" {
      t = MigrationDown
    }

    contents, err = ioutil.ReadFile(path.Join(migrationsPath, fi.Name()))
    if err != nil { return nil, err }

    num, err = strconv.ParseUint(matches[1], 10, 64)
    if err != nil { return nil, err }
    
    migrations = append(migrations, &Migration{
      N: num,
      Name: matches[2],
      Direction: t,
      Data: contents,
    })
  }

  return migrations, nil
}

func createVersionsTable(pg *pgx.Conn) error {
  _, err := pg.Exec(context.Background(), `
  create table if not exists versions (
    current_version    integer   not null default 0
  );
  `)
  return err
}

func getCurrentVersion(pg *pgx.Conn) (int, bool) {
  row := pg.QueryRow(context.Background(), `
  select current_version from versions limit 1;
  `)
  var version int
  err := row.Scan(&version)
  if err != nil && err.Error() == "no rows in result set" {
    return 0, false
  } else if err != nil {
    log.Println("error while getting version from postgres")
    log.Println(err)
    return 0, false
  }
  return version, true
}

func setCurrentVersion(pg *pgx.Conn, version int) error {
  row := pg.QueryRow(context.Background(), `
  delete from versions;
  `)
  err := row.Scan()
  if err != nil && err.Error() != "no rows in result set" {
    return err
  }
  row = pg.QueryRow(context.Background(), `
  insert into versions (current_version) values ($1);
  `, version)
  err = row.Scan()
  if err != nil && err.Error() != "no rows in result set" {
    return err
  }
  return nil
}

func applyMigration(pg *pgx.Conn, migration *Migration) error {
  tx, err := pg.Begin(context.Background())
  if err != nil { return err }
  
  defer tx.Rollback(context.Background())

  _, err = tx.Exec(context.Background(), string(migration.Data))
  if err != nil {
    return err
  }

  err = tx.Commit(context.Background())
  if err != nil {
    return err
  }
  return nil
}
