package log

import (
	"os"

	"github.com/sirupsen/logrus"
)

func New() *logrus.Logger {
	l := logrus.New()

	configureLogger(l)

	return l
}

func configureLogger(l *logrus.Logger) {
	var reportCaller = os.Getenv("REPORT_CALLER")
	if reportCaller == "true" {
		l.SetReportCaller(true)
	}

	var environment = os.Getenv("ENVIRONMENT")
	if environment == "production" {
		l.SetLevel(logrus.InfoLevel)
		l.SetFormatter(&logrus.JSONFormatter{})
	} else {
		l.SetLevel(logrus.TraceLevel)
		l.SetFormatter(&logrus.TextFormatter{})
	}

	l.SetOutput(os.Stdout)
}
