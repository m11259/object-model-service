package utils

import (
	"github.com/sirupsen/logrus"
	// "google.golang.org/protobuf/encoding/protowire"
	"google.golang.org/protobuf/reflect/protoreflect"
)

func RepresentProtoMesssageToLogrusFields(msg protoreflect.Message) *logrus.Fields {
  // desc := msg.Descriptor()
  // n := desc.Fields().Len()
  lf := logrus.Fields{}

  msg.Range(func(fd protoreflect.FieldDescriptor, v protoreflect.Value) bool {
    lf[fd.TextName()] = v.Interface()
    return true
  })

  // var fieldDescriptor protoreflect.FieldDescriptor
  // for i := 0; i <= n; i++ {
  //   fieldDescriptor = desc.Fields().ByNumber(protowire.Number(i))
  //   if !fieldDescriptor.Name().IsValid() {
  //     continue
  //   }
  //   lf[fieldDescriptor.TextName()] = msg.Get(fieldDescriptor).Interface()
  // }
  return &lf
}
