package utils

import (
	"errors"
	"fmt"
	"strings"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// MapPGXError maps errors of postgres to the following set
//  - unknown database error
//  - database connection error
//  - feature not supported
//  - data exception
//  - integrity:
//    - unique violation
//    - not null violation
//    - foreign key violation
//    - unique violation
//
func MapPGXError(err error) error {
  if errors.Is(err, pgx.ErrNoRows) {
    return errors.New("empty result")
  }

  var pgError *pgconn.PgError
  if errors.As(err, &pgError) {
    // https://www.postgresql.org/docs/current/errcodes-appendix.html
    switch {
    case strings.HasPrefix(pgError.Code, "08"):
      return errors.New("database connection error")

    case strings.HasPrefix(pgError.Code, "22"):
      return errors.New("data exception")

    case strings.HasPrefix(pgError.Code, "23000"):
      return errors.New("integrity constraint violation")
    case strings.HasPrefix(pgError.Code, "23001"):
      return errors.New("restrict violation")
    case strings.HasPrefix(pgError.Code, "23502"):
      return errors.New("not null violation")
    case strings.HasPrefix(pgError.Code, "23503"):
      return errors.New("foreign key violation")
    case strings.HasPrefix(pgError.Code, "23505"):
      return errors.New("unique violation")

    case strings.HasPrefix(pgError.Code, "53"):
      return errors.New("insufficient resources")

    case strings.HasPrefix(pgError.Code, "58"):
      return errors.New("system error")
    }
  }
  return errors.New("unknown database error")
}

func ConvertPGXToStatusError(err error) error {
  mappedError := MapPGXError(err)

  switch mappedError.Error() {
  case "database connection error", "system error":
    return status.Error(codes.Unavailable, "database is not available")
  case "insufficient resources":
    return status.Error(codes.ResourceExhausted, "resource exhausted")
  case "data exception":
    return status.Error(codes.InvalidArgument, "data exception")

  case "unique violation":
    return status.Error(codes.AlreadyExists, "the object already exists")

  case "foreign key violation":
    return status.Error(codes.InvalidArgument, "possibly invalid id of the pointing object")

  case "not null violation":
    return status.Error(codes.InvalidArgument, "null argument")

  case "integrity constraint violation", "restrict violation":
    return status.Error(codes.InvalidArgument, "constraint violation")

  case "empty result":
    return status.Error(codes.NotFound, "no result")

  default:
    return status.Error(codes.Internal, "internal error")
  }
}
