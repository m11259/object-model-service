package broker

type Client struct {
	name   string
	topics []string
	buffer ClientEventBuffer
}

func NewClient(name string, buffer ClientEventBuffer) *Client {
	return &Client{
		name:   name,
		topics: make([]string, 1),
		buffer: buffer,
	}
}

func (c *Client) Name() string {
	return c.name
}

func (c *Client) IsSubscribedTo(name string) bool {
	for _, v := range c.topics {
		if name == v {
			return true
		}
	}

	return false
}

func (c *Client) Subscribe(name ...string) {
	c.topics = append(c.topics, name...)
}

func (c *Client) Unsubscribe(name string) {
	for i, v := range c.topics {
		if name == v {
			c.topics = append(c.topics[:i], c.topics[i+1:]...)
		}
	}
}

func (c *Client) UnsubscribeAll() {
	c.topics = make([]string, 0)
}

func (c *Client) Buffer() ClientEventBuffer {
	return c.buffer
}
