package broker

type Event struct {
	Topic string
	Data  []byte
}

type Broker interface {
	Start() error
	Stop() error
	Emit(topic string, data []byte) error
	AddClient(name string) *Client
	GetClient(name string) *Client
}
