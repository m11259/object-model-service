package broker

type MemoryClientEventBuffer struct {
	events chan Event
}

func NewMemoryClientEventBuffer() *MemoryClientEventBuffer {
	return &MemoryClientEventBuffer{
		events: make(chan Event),
	}
}

func (c *MemoryClientEventBuffer) Send(ev Event) {
	c.events <- ev
}

func (c *MemoryClientEventBuffer) ReadChannel() <-chan Event {
	return c.events
}
