package broker

type MemoryBroker struct {
	events  chan Event
	clients map[string]*Client
	done    chan interface{}
}

func NewMemoryBroker() *MemoryBroker {
	brocker := &MemoryBroker{
		events:  make(chan Event),
		clients: make(map[string]*Client),
		done:    make(chan interface{}),
	}

	return brocker
}

func (m *MemoryBroker) Start() error {
	for {
		select {
		case <-m.done:
			return nil
		case event := <-m.events:
			for _, client := range m.clients {
				if client.IsSubscribedTo(event.Topic) {
					client.Buffer().Send(event)
				}
			}
		}
	}
}

func (m *MemoryBroker) Stop() error {
	m.done <- true

	return nil
}

func (m *MemoryBroker) Emit(topic string, data []byte) error {
	m.events <- Event{
		Topic: topic,
		Data:  data,
	}

	return nil
}

func (m *MemoryBroker) AddClient(name string) *Client {
	client := NewClient(name, NewMemoryClientEventBuffer())
	m.clients[name] = client

	return client
}

func (m *MemoryBroker) GetClient(name string) *Client {
	client := m.clients[name]

	return client
}
