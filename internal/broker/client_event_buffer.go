package broker

type ClientEventBuffer interface {
	Send(ev Event)
	ReadChannel() <-chan Event
}
