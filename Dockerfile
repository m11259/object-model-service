FROM golang:1.18 as builder
RUN mkdir /app 
ADD . /app/ 
WORKDIR /app 
ENV GO111MODULE=on
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main ./cmd/server/server.go ./cmd/server/grpc.go

FROM alpine
RUN apk add --no-cache gcompat
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app/main /main
COPY sql/migrations /migrations
EXPOSE 9876
CMD ["/main"]

