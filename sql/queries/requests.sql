-- name: CreateRequest :one
insert into requests(user_id, body, user_address, car_number)
values ($1, $2, $3, $4)
returning *;

-- name: RetrieveRequests :many
select
  requests.id as id,
  requests.user_id as user_id,
  requests.body as body,
  requests.user_address as address,
  requests.car_number as car_number,
  requests.created_at as created_at,
  requests.status as status,
  requests.pass as pass,
  requests.reviewed_at as reviewed_at,
  requests.fulfilled_at as fulfilled_at,
  users.username as user_username,
  users.telegramname as user_telegramname,
  users.firstname as user_firstname,
  users.lastname as user_lastname,
  users.phone as user_phone,
  users.address as user_address,
  users.address_confirmed as user_address_confirmed,
  users.car_numbers as user_car_numbers,
  users.identified as user_identified,
  users.activated as user_activated,
  users.lang as user_lang,
  users.created_at as user_created_at,
  users.updated_at as user_updated_at
from requests left join users on users.id = requests.user_id
where 
  case when sqlc.narg('user_id')::bigint is null then true
    else sqlc.narg('user_id') = user_id
  end
  and
  case when sqlc.narg('body_contains')::text is null then true
    else lower(requests.body) like '%' || lower(sqlc.narg('body_contains')) || '%'
  end
  and
  case when sqlc.narg('user_address_contains')::text is null then true
    else user_address like '%' || sqlc.narg('user_address_contains') || '%'
  end
  and
  case when sqlc.narg('user_address')::text is null then true
    else sqlc.narg('user_address') = user_address
  end
  and
  case when sqlc.narg('car_number_contains')::text is null then true
    else car_number like '%' || sqlc.narg('car_number_contains') || '%'
  end
  and
  case when sqlc.narg('car_number')::text is null then true
    else sqlc.narg('car_number') = car_number
  end
  and
  case
    when sqlc.narg('created_at_from')::timestamp is not null and
      sqlc.narg('created_at_to')::timestamp is not null then
      sqlc.narg('created_at_from')::timestamp <= requests.created_at and
      sqlc.narg('created_at_to')::timestamp >= requests.created_at
    else true
  end
  and
  case
    when sqlc.narg('reviewed_at_from')::timestamp is not null and
      sqlc.narg('reviewed_at_to')::timestamp is not null then
      sqlc.narg('reviewed_at_from')::timestamp <= reviewed_at and
      sqlc.narg('reviewed_at_to')::timestamp >= reviewed_at
    else true
  end
  and
  case
    when sqlc.narg('fulfilled_at_from')::timestamp is not null and
      sqlc.narg('fulfilled_at_to')::timestamp is not null then
      sqlc.narg('fulfilled_at_from')::timestamp <= fulfilled_at and
      sqlc.narg('fulfilled_at_to')::timestamp >= fulfilled_at
    else true
  end
  and
  case when sqlc.narg('status')::int is null then true
    else sqlc.narg('status')::int = status
  end
order by created_at desc
limit $1 offset $2 ;

-- name: GetRequestsPending :many
select
  requests.id as id,
  requests.user_id as user_id,
  requests.body as body,
  requests.user_address as address,
  requests.car_number as car_number,
  requests.created_at as created_at,
  requests.status as status,
  requests.pass as pass,
  requests.reviewed_at as reviewed_at,
  requests.fulfilled_at as fulfilled_at,
  users.username as user_username,
  users.telegramname as user_telegramname,
  users.firstname as user_firstname,
  users.lastname as user_lastname,
  users.phone as user_phone,
  users.address as user_address,
  users.address_confirmed as user_address_confirmed,
  users.car_numbers as user_car_numbers,
  users.identified as user_identified,
  users.activated as user_activated,
  users.lang as user_lang,
  users.created_at as user_created_at,
  users.updated_at as user_updated_at
from requests left join users on requests.user_id = users.id
  where status = 0 order by requests.created_at desc;

-- name: GetRequestsReviewed :many
select
  requests.id as id,
  requests.user_id as user_id,
  requests.body as body,
  requests.user_address as address,
  requests.car_number as car_number,
  requests.created_at as created_at,
  requests.status as status,
  requests.pass as pass,
  requests.reviewed_at as reviewed_at,
  requests.fulfilled_at as fulfilled_at,
  users.username as user_username,
  users.telegramname as user_telegramname,
  users.firstname as user_firstname,
  users.lastname as user_lastname,
  users.phone as user_phone,
  users.address as user_address,
  users.address_confirmed as user_address_confirmed,
  users.car_numbers as user_car_numbers,
  users.identified as user_identified,
  users.activated as user_activated,
  users.lang as user_lang,
  users.created_at as user_created_at,
  users.updated_at as user_updated_at
from requests left join users on requests.user_id = users.id
  where requests.status != 0 order by requests.created_at desc
  limit $1 offset $2;

-- name: GetRequestsReviewedFromTo :many
select requests.id, requests.body, case requests.status when 1 then true else false end as accepted,
  users.firstname, users.lastname, users.username, users.phone, requests.user_address, requests.created_at, requests.reviewed_at
  from requests left join users on users.id = requests.user_id or requests.user_id = null
  where requests.status != 0 and requests.created_at >= $1 and requests.created_at <= $2;

-- name: GetRequestsReviewedLast :many
select
  requests.id as id,
  requests.user_id as user_id,
  requests.body as body,
  requests.user_address as address,
  requests.car_number as car_number,
  requests.created_at as created_at,
  requests.status as status,
  requests.pass as pass,
  requests.reviewed_at as reviewed_at,
  requests.fulfilled_at as fulfilled_at,
  users.username as user_username,
  users.telegramname as user_telegramname,
  users.firstname as user_firstname,
  users.lastname as user_lastname,
  users.phone as user_phone,
  users.address as user_address,
  users.address_confirmed as user_address_confirmed,
  users.car_numbers as user_car_numbers,
  users.identified as user_identified,
  users.activated as user_activated,
  users.lang as user_lang,
  users.created_at as user_created_at,
  users.updated_at as user_updated_at
from requests left join users on requests.user_id = users.id
  where requests.status != 0 order by requests.reviewed_at desc
  limit $1;

-- name: GetRequestById :one
select
  requests.id as id,
  requests.user_id as user_id,
  requests.body as body,
  requests.user_address as address,
  requests.car_number as car_number,
  requests.created_at as created_at,
  requests.status as status,
  requests.pass as pass,
  requests.reviewed_at as reviewed_at,
  requests.fulfilled_at as fulfilled_at,
  users.username as user_username,
  users.telegramname as user_telegramname,
  users.firstname as user_firstname,
  users.lastname as user_lastname,
  users.phone as user_phone,
  users.address as user_address,
  users.address_confirmed as user_address_confirmed,
  users.car_numbers as user_car_numbers,
  users.identified as user_identified,
  users.activated as user_activated,
  users.lang as user_lang,
  users.created_at as user_created_at,
  users.updated_at as user_updated_at
from requests left join users on requests.user_id = users.id
  where requests.id = $1;

-- name: GetRequestsByUser :many
select * from requests where user_id=$1 and status=0 ORDER BY id LIMIT 3 OFFSET $2;

-- name: UpdateRequestStatus :one
update requests
set status = coalesce(sqlc.narg('status'), status),
    reviewed_at = coalesce(sqlc.narg('reviewed_at'), reviewed_at),
    fulfilled_at = coalesce(sqlc.narg('fulfilled_at'), fulfilled_at)
where requests.id = $1
returning id;

-- name: UpdateRequestPass :one
update requests
set pass = $2
where requests.id = $1
returning 
  requests.id as id;

 -- name: DeleteRequest :one
 delete from requests where id=$1 returning *;
