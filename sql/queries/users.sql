-- name: CreateUser :one
insert into Users (id, username, telegramname, firstname,
  lastname, phone, address, address_confirmed, activated,
  identified, car_numbers, lang)
values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
on conflict (id)
  do update
  set username=$2, telegramname=$3, firstname=$4,
    lastname=$5, phone=$6, address=$7, address_confirmed=$8,
    activated=$9, identified=$10, car_numbers=$11, lang=$12,
    updated_at=now()
returning *;

-- name: GetUsers :many
select *
from users
order by updated_at desc 
LIMIT $1 OFFSET $2;

-- name: GetUnidentifiedUsers :many
select *
from users
where identified = false
order by updated_at desc
LIMIT $1 OFFSET $2;

-- name: CountUnidentified :one
select count(*)
from users
where identified = false;

-- name: GetUserById :one
select *
from users
where id = $1;

-- name: RetrieveUsers :many
select *
from users
where
  case when sqlc.narg('username')::text is null then true
    else username like '%' || sqlc.narg('username') || '%'
  end
  and
  case when sqlc.narg('telegramname')::text is null then true
    else telegramname like '%' || sqlc.narg('telegramname') || '%'
  end
  and
  case when sqlc.narg('firstname')::text is null then true
    else firstname like '%' || sqlc.narg('firstname') || '%'
  end
  and
  case when sqlc.narg('lastname')::text is null then true
    else lastname like '%' || sqlc.narg('lastname') || '%'
  end
  and
  case when sqlc.narg('phone')::text is null then true
    else phone like '%' || sqlc.narg('phone') || '%'
  end
  and
  case when sqlc.narg('address')::text is null then true
    else address like '%' || sqlc.narg('address') || '%'
  end
  and
  case when sqlc.narg('address_confirmed')::bool is null then true
    else sqlc.narg('address_confirmed')::bool = address_confirmed
  end
  and
  case when sqlc.narg('car_numbers')::text[] is null then true
    else sqlc.narg('car_numbers')::text[] && car_numbers
  end
  and
  case when sqlc.narg('identified')::bool is null then true
    else sqlc.narg('identified')::bool = identified
  end
  and
  case when sqlc.narg('activated')::bool is null then true
    else sqlc.narg('activated')::bool = activated
  end
  and
  case when sqlc.narg('lang')::text is null then true
    else sqlc.narg('lang') = lang
  end
order by updated_at desc
limit $1 offset $2 ;

-- name: GetUserByUsername :one
select *
from users
where username = $1;

-- name: GetUserByRequest :one
select users.*
from users
  join requests
  on requests.user_id = users.id
  where requests.user_id = $1;

-- name: UpdateUser :one
update users
set
    firstname = coalesce(sqlc.narg('firstname'), firstname),
    lastname = coalesce(sqlc.narg('lastname'), lastname),
    phone = coalesce(sqlc.narg('phone'), phone),
    address = coalesce(sqlc.narg('address'), address),
    address_confirmed = coalesce(sqlc.narg('address_confirmed'), address_confirmed),
    identified = coalesce(sqlc.narg('identified'), identified),
    activated = coalesce(sqlc.narg('activated'), activated),
    lang = coalesce(sqlc.narg('lang'), lang),
    updated_at = now()
where id = $1
returning *;

-- name: DeleteUser :one
delete from users
where id = $1
returning *;
