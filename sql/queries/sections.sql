-- name: GetAllSections :many
select * from sections;

-- name: CreateSection :one
insert into sections(name, data)
values($1, $2)
returning *;

-- name: UpdateSection :one
update sections
set name = coalesce(sqlc.narg('name'), name),
    data = coalesce(sqlc.narg('data'), data)
where id = sqlc.narg('id')
returning *;

-- name: DeleteSection :one
delete from sections
where id = $1
returning *;
