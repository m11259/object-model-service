-- name: CreateAdmin :one
insert into admins(username, password) 
values ($1, $2)
on conflict do nothing
returning *;

-- name: GetAllAdmins :many
select *
from admins 
order by created_at desc;

-- name: UpdateAdmin :one
update admins
set username = coalesce(sqlc.narg('username'), username),
    password = coalesce(sqlc.narg('password'), password),
    updated_at = now()
where id = sqlc.arg('id')
returning *;

-- name: DeleteAdmin :one
delete from admins
where id = sqlc.arg('id')
returning *;
