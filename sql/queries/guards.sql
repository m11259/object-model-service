-- name: GetGuardsList :many
select guards.id, username, sections.name as section_name,
sections.id as section_id, sections.data as section_data, created_at 
  from guards left join sections
  on guards.section_id = sections.id
  ORDER BY created_at DESC;

-- name: GetGuardByUsername :one
select guards.id, guards.username, guards.created_at, guards.updated_at,
  sections.id as section_id, sections.data as section_data,
  sections.name as section_name
from guards left join sections
on guards.section_id = sections.id
where guards.username = $1;

-- name: GetGuardById :one
select guards.id, guards.username, guards.created_at, guards.updated_at,
  sections.id as section_id, sections.data as section_data,
  sections.name as section_name
from guards left join sections
on guards.section_id = sections.id
where guards.id = $1;

-- name: CreateGuard :one
insert into Guards(username, password)
	values($1, $2)
	returning *;

-- name: UpdateGuardUsername :one
update guards
set username = $2
where id = $1
returning *;

-- name: UpdateGuardPassword :one
update guards
set password = $2
where id = $1
returning *;

-- name: UpdateGuard :one
update guards
set username   = coalesce(sqlc.narg('username'), username),
    password   = coalesce(sqlc.narg('password'), password),
    section_id = coalesce(sqlc.narg('section_id')::bigint, section_id),
    updated_at = now()
from (
  select sections.id as section_id, sections.name as section_name,
    sections.data as section_data
  from guards join sections on guards.section_id = sections.id
  where guards.id = $1
  for update
  ) sub
where guards.id = $1
returning guards.id, guards.username, guards.created_at, guards.updated_at,
  sub.section_id, sub.section_name, sub.section_data;

-- name: DeleteGuard :one
delete from guards
-- но тут ебаный using как-то не референсится в returning в sqlc
using (
  select guards.id, guards.username, guards.password, guards.created_at,
    guards.updated_at, sections.id as section_id,
    sections.name as section_name, sections.data as section_data
  from guards join sections on guards.section_id = sections.id
  where guards.id = $1
) sub
where guards.id = $1
returning *;

-- name: GetGuardSection :one
select sections.*
from sections
join guards on guards.section_id=sections.id
where guards.id = $1;

-- name: UpdateGuardSection :one
update guards
set section_id=$2
where id = $1
returning *; 
