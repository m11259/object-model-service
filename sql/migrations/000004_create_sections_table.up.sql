CREATE TABLE IF NOT EXISTS Sections (
  id    bigserial   PRIMARY KEY,
  name 	text 		    UNIQUE NOT NULL,
  data	jsonb		    NOT NULL
);
