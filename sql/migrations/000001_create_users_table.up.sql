CREATE TABLE IF NOT EXISTS Users (
  id                bigint          PRIMARY KEY,
  username          varchar(255)    ,
  telegramname      varchar(255)    not null,
  firstname         varchar(255)    not null DEFAULT '',
  lastname          varchar(255)    ,
  phone             varchar(15)     not null,
  address	    	    varchar(255)    not null default '',
  address_confirmed boolean         not null DEFAULT FALSE,
  car_numbers       text[]          ,
  identified        boolean         DEFAULT FALSE,
  activated         boolean         DEFAULT FALSE,
  lang              varchar(3)      not null DEFAULT 'uk',
  created_at        timestamp with time zone       DEFAULT now(),
  updated_at        timestamp with time zone       DEFAULT now()
);
