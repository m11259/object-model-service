CREATE TABLE IF NOT EXISTS Requests (
  id           bigserial       PRIMARY KEY,
  user_id      bigint          REFERENCES Users(id),
  body         text            NOT NULL,
  user_address text			       ,
  car_number   text            , 
  created_at   timestamp with time zone       NOT NULL DEFAULT now(),
  status       integer         NOT NULL DEFAULT 0,
  pass         jsonb           ,
  reviewed_at  timestamp with time zone      ,      
  fulfilled_at timestamp with time zone
);
