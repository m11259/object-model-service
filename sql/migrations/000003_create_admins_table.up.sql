CREATE TABLE IF NOT EXISTS Admins (
  id          bigserial       PRIMARY KEY,
  username    text            UNIQUE NOT NULL,
  "password"  text            NOT NULL,
  created_at  timestamp with time zone      NOT NULL DEFAULT now(),
  updated_at  timestamp with time zone      not null default now()
);
