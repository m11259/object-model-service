# Event Service Topics Suggestion

## Reserved

| Entity  | Topic                       | Description             |
|---------|-----------------------------|-------------------------|
| `user`  | `user/create`               | Data of created user    |
|         | `user/update`               | Data of updated user    |
|         | `user/identify`             |                         |
|         | `user/unidentify`           |                         |
|         | `user/activate`             |                         |
|         | `user/deactivate`           |                         |
|         | `user/payment`              | sends data about proceeded payments by user through telegram payments api |
|         | `user/payment_notification` | sends data with notifications about due payments |
|`request`| `request/create`            | created request         |
|         | `request/update`            | update of body or car number |
|         | `request/accept`            | guard marks the request as done (the car is on the territory) |
|         | `request/fulfill`           | the car has left the territory |
|         | `request/deny`              | guard denies the request |
|         | `request/undeny`            | guard sets the request active again |
|         | `request/expire`            | request lasted active too long and expires |


not yet resolved event streams

- `user/payment` sends data about proceeded payments by user through telegram payments api
- `user/payment_notification` sends data with notifications about due payments

Also each service may listen to its own stream nammed respectively.
E.g. service `api` may listen to `api` stream.


