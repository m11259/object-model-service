# Linting

Firstly install [golangci-lint](https://golangci-lint.run/)
```bash
make install-go-lint
```
Then linter should be run before submition of main branch changes
```bash
make lint
```
or simply
```bash
golangci-lint run
```
Read linting errors, analyse and fix them.

## WSL

[Rules](https://github.com/bombsimon/wsl/blob/master/doc/rules.md)
