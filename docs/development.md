# OMS Development Guide
---

## Migrations

For generation of sql-queries and table to golang structure - [sqlc](https://github.com/kyleconroy/sqlc).

### How to create new migrations file?

1. Create two files for migration up and down in `sql/migrations` directory
using template
`{version number, 6 digits}_{migration name}.down.sql`
and
`{version number, 6 digits}_{migration name}.up.sql`

### How to generate queries and tables?

Queries are generated from `sql/queries` directory.
Tables are generated from `sql/migrations` directory.

```bash
make sqlc
```

---


## Protobuf

### Arch linux `protobuf` installation
```bash
yay -S protobuf
```

### Go plugins
Go plugins for the protocol compiler
```bash
go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
```

### How to generate code out of proto files?
```bash
make protoc
```

