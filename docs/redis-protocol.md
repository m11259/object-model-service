# Redis protocol

What redis (or calls that take data from redis) is responsible for:
- [x] list of available addresses
- [x] payments by date/month
- [x] payment quota by address
- [x] unpaid months by address
- [x] has user paid in last n moths
- [ ] disable user (may be decided by api or worker) - STORED IN POSTGRES
- [x] disable users by address
- [x] store addresses that are disabled
- [x] metadata per address:
  - [x] vrn: vehicle registration number
  - [x] phone numbers
  - [x] other metadata

## Realization in GRPC

- [ ] List addresses
- [ ] Check address existance
- [ ] Get/Set address payment quota
- [ ] Get/Set default payment quota


## Redis Data Schema

### Addresses and Metadata

- [x] List all addresses in key `addresses` in a format of set.

#### Metadata

- [x] Store metadata of address in `addresses:metadata:<address>` in a hash data type

| Hash key | Hash value                                             |
|----------|--------------------------------------------------------|
| `vrn`    | `AB4444EA;AC4838TI`                                    |
| `phones` | `+380984737443;+380954483743`                          |
| `guests` | `Sabrina Nikolaenko`                                   |
| `notes`  | `Хороший житель, только бусурманин... Не понимаем их.` |

#### Disabling

each address is either active or disabled.
- [x] disabled addresses are stored in `addresses:disabled` key with a set data type

### Payments

- [x] store paid/unpaid month in `payments:stats:<year>:<month>` hash with key as address

- [x] store status (paid ok, paid not ok) as 1 or 0 value in
a key `payments:status:<year>:<month>:<address>`

- [x] store payment quota in `payments:quota:<year>:<month>` for addresses which quota
is not default. `<year>` and `<month>` should be as we must remeber quota changes

- [x] default quota is in `payments:quota:default:<year>:<month>`

- [x] unpaid months are stored in a format of debts `payments:debts:<year>:<month>:<address>`

| Key                          | Value    |
|------------------------------|----------|
| `payments:debts:2022:07:234` | `233.10` |
