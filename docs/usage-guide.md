# GRPC
---
## Table of Contents
- [Notes](#notes)
- [Admin](#admin)
- [Guard](#guard)
- [Request](#request)
- [Section](#section)
- [User](#user)
- [Events](#events)
  - [Calls](#calls)
    - [Create client](#create-client)
    - [Subscribe](#subscribe)
    - [Emit](#emit)

---

## Notes
##### Metadata
> `grpc-caller-service` metadata is used for logging. It is mandatory
to set this value to a reasonable and unique name.

The exception `No metadata provided` is thrown in case when the metadata
is not provided to the method that needs it for observability.

##### Timestamp
> Taken from [google protobufs](https://github.com/protocolbuffers/protobuf-go/blob/master/types/known/timestamppb/timestamp.pb.go)

A Timestamp represents a point in time independent of any time zone or local
calendar, encoded as a count of seconds and fractions of seconds at
nanosecond resolution. The count is relative to an epoch at UTC midnight on
January 1, 1970, in the proleptic Gregorian calendar which extends the
Gregorian calendar backwards to year one.

All minutes are 60 seconds long. Leap seconds are "smeared" so that no leap
second table is needed for interpretation, using a [24-hour linear
smear](https://developers.google.com/time/smear).

The range is from `0001-01-01T00:00:00Z` to `9999-12-31T23:59:59.999999999Z`. By
restricting to that range, we ensure that we can convert to and from [RFC
3339](https://www.ietf.org/rfc/rfc3339.txt) date strings.
###### JSON Mapping

In JSON format, the Timestamp type is encoded as a string in the
[RFC 3339](https://www.ietf.org/rfc/rfc3339.txt) format. That is, the
format is `{year}-{month}-{day}T{hour}:{min}:{sec}[.{frac_sec}]Z`
where `{year}` is always expressed using four digits while `{month}`, `{day}`,
`{hour}`, `{min}`, and `{sec}` are zero-padded to two digits each. The fractional
seconds, which can go up to 9 digits (i.e. up to 1 nanosecond resolution),
are optional. The "Z" suffix indicates the timezone ("UTC"); the timezone
is required. A proto3 JSON serializer should always use UTC (as indicated by
"Z") when printing the Timestamp type and a proto3 JSON parser should be
able to accept both UTC and other timezones (as indicated by an offset).

For example, `2017-01-15T01:30:15.01Z` encodes 15.01 seconds past
01:30 UTC on January 15, 2017.

###### JavaScript
In JavaScript, one can convert a Date object to this format
using the standard
[toISOString()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString)
method.

```go
type Timestamp struct {
  // Represents seconds of UTC time since Unix epoch
  // 1970-01-01T00:00:00Z. Must be from 0001-01-01T00:00:00Z to
  // 9999-12-31T23:59:59Z inclusive.
  Seconds int64 `protobuf:"varint,1,opt,name=seconds,proto3" json:"seconds,omitempty"`
  // Non-negative fractions of a second at nanosecond resolution. Negative
  // second values with fractions must still have non-negative nanos values
  // that count forward in time. Must be from 0 to 999,999,999
  // inclusive.
  Nanos int32 `protobuf:"varint,2,opt,name=nanos,proto3" json:"nanos,omitempty"`
}
```


## Admin

### CreateAdmin

Call: `CreateAdmin`

Metadata: `grpc-caller-service: <name of the caller>`

##### Request
```
Username: string
Password: string
```

##### Response
```
Id: int64
Username: string
```

##### Throws
- `empty username or password`
- other errors of the database (provided as they are)

---


## Guard

### CreateGuard

Call: `CreateGuard`

Metadata: `grpc-caller-service: <name of the caller>`

##### Request
```
Username: string
Password: string
```

##### Response
```
Id: int64
Username: string
CreatedAt: {
    Seconds: string
    Nanos: number
  }
```

##### Throws
- `empty username`
- `empty password`
- other errors of the database (provided as they are)

### GetAllGuards
Call: `GetAllGuards`

Metadata: `grpc-caller-service: <name of the caller>`

##### Response
```
List [
  Id        int64
  Username  string
  Section   {
      Id   int64
      Name string
      Data string
    }
  CreatedAt timestamppb.Timestamp
]
```

### GetGuard
Call: `GetGuard`

Metadata: `grpc-caller-service: <name of the caller>`

`grpc-caller-service` metadata is used for logging. It is mandatory
to set this value to a reasonable and unique name.

##### Request
Specify either `Id` or `Username`.
```
Id       int64 optional
Username string optional
```

##### Throws
- other errors of the database (provided as they are)

##### Response
```
Id        int64
Username  string
Section   {
    Id   int64
    Name string
    Data string
  }
CreatedAt timestamppb.Timestamp
```

### UpdateGuard
Call: `UpdateGuard`

Metadata: `grpc-caller-service: <name of the caller>`

`grpc-caller-service` metadata is used for logging. It is mandatory
to set this value to a reasonable and unique name.

Specify the fields to update in request. If the field is not
present then the value of the field remains untouched.

##### Request
```
Id        int64
Username  string optional
Password  string optional
SectionId int64 optional
```

##### Response
```
Id        int64
Username  string
Section   {
    Id   int64
    Name string
    Data string
  }
CreatedAt timestamppb.Timestamp
```

##### Throws
- other errors of the database (provided as they are)

### DeleteGuard
Call: `DeleteGuard`

Metadata: `grpc-caller-service: <name of the caller>`

`grpc-caller-service` metadata is used for logging. It is mandatory
to set this value to a reasonable and unique name.

##### Request
```
Id int64
```

##### Response
```
Id        int64
Username  string
CreatedAt timestamppb.Timestamp
```
##### Throws
- other errors of the database (provided as they are)

---

## Request

### Create Request

Call: `CreateRequest`

Metadata: `grpc-caller-service: <name of the caller>`

---

## Section

### CreateSection

Call: `CreateSection`

Metadata: `grpc-caller-service: <name of the caller>`

##### Request
```
Name string
Data string
```

##### Response
```
Id   int64
Name string
Data string
```
##### Throws
- other errors of the database (provided as they are)

### GetSections

Call: `GetSections`

Metadata: `grpc-caller-service: <name of the caller>`

##### Response
```
Sections [
  Id   int64
  Name string
  Data string
]
```
##### Throws
- other errors of the database (provided as they are)

### UpdateSection

Call: `UpdateSection`

Metadata: `grpc-caller-service: <name of the caller>`

Specify the fields to update in request. If the field is not
present then the value of the field remains untouched.

##### Request
```
Id   int64
Name string optional
Data string optional
```

##### Response
```
Id   int64
Name string
Data string
```
##### Throws
- other errors of the database (provided as they are)

### DeleteSection

Call: `DeleteSection`

Metadata: `grpc-caller-service: <name of the caller>`

##### Request
```
Id int64
```

##### Response
```
Id   int64
Name string
Data string
```
##### Throws
- other errors of the database (provided as they are)

---

## User

### CreateUser
Call: `CreateUser`

Metadata: `grpc-caller-service: <name of the caller>`

##### Request
```
Id               int64
Telegramname     string
Username         string optional
Firstname        string
Lastname         string optional
Phone            string
Address          string
AddressConfirmed bool
Activated        bool
Identified       bool
CarNumbers       []string
Lang             string optional
```

##### Response
```
Id               int64
Username         string optional
Telegramname     string
Firstname        string
Lastname         string optional
Phone            string
Address          string
CarNumbers       []string
AddressConfirmed bool
Identified       bool
Activated        bool
Lang             string
CreatedAt        timestamppb.Timestamp
UpdatedAt        timestamppb.Timestamp
```
##### Throws
- other errors of the database (provided as they are)

### GetAllUsers
Call: `GetAllUsers`

Metadata: `grpc-caller-service: <name of the caller>`

##### Request
```
Limit  int32
Offset int32
```

##### Response
```
Users [
  Id               int64
  Username         string optional
  Telegramname     string
  Firstname        string
  Lastname         string optional
  Phone            string
  Address          string
  CarNumbers       []string
  AddressConfirmed bool
  Identified       bool
  Activated        bool
  Lang             string
  CreatedAt        timestamppb.Timestamp
  UpdatedAt        timestamppb.Timestamp
]
```
##### Throws
- other errors of the database (provided as they are)


### GetUnidentifiedUsers
Call: `GetUnidentifiedUsers`

Metadata: `grpc-caller-service: <name of the caller>`
##### Request
```
Limit  int32
Offset int32
```

##### Response
```
Users [
  Id               int64
  Username         string optional
  Telegramname     string
  Firstname        string
  Lastname         string optional
  Phone            string
  Address          string
  CarNumbers       []string
  AddressConfirmed bool
  Identified       bool
  Activated        bool
  Lang             string
  CreatedAt        timestamppb.Timestamp
  UpdatedAt        timestamppb.Timestamp
]
```
##### Throws
- other errors of the database (provided as they are)

### GetUser
Call: `GetUser`

Metadata: `grpc-caller-service: <name of the caller>`
##### Request
```
Id       int64 optional
Username string optional
```

##### Response
```
Id               int64
Username         string optional
Telegramname     string
Firstname        string
Lastname         string optional
Phone            string
Address          string
CarNumbers       []string
AddressConfirmed bool
Identified       bool
Activated        bool
Lang             string
CreatedAt        timestamppb.Timestamp
UpdatedAt        timestamppb.Timestamp
```
##### Throws
- other errors of the database (provided as they are)


### RetrieveUsers
Call: `RetrieveUsers`

Metadata: `grpc-caller-service: <name of the caller>`
##### Request
```
Limit                 int32
Offset                int32
UsernameSubstring     *string
TelegramnameSubstring *string
FirstnameSubstring    *string
LastnameSubstring     *string
PhoneSubstring        *string
AddressSubstring      *string
AddressConfirmed      *bool
CarNumbers            []string
Identified            *bool
Activated             *bool
Lang                  *string
```

##### Response
```
Users [
  Id               int64
  Username         string optional
  Telegramname     string
  Firstname        string
  Lastname         string optional
  Phone            string
  Address          string
  CarNumbers       []string
  AddressConfirmed bool
  Identified       bool
  Activated        bool
  Lang             string
  CreatedAt        timestamppb.Timestamp
  UpdatedAt        timestamppb.Timestamp
]
```
##### Throws
- other errors of the database (provided as they are)


### UpdateUser
Call: `UpdateUser`

Metadata: `grpc-caller-service: <name of the caller>`
##### Request
```
Id               int64
Firstname        string optional
Lastname         string optional
Phone            string optional
Address          string optional
AddressConfirmed bool   optional
Identified       bool   optional
Activated        bool   optional
Lang             string optional
```

##### Response
```
Id               int64
Username         string optional
Telegramname     string
Firstname        string
Lastname         string optional
Phone            string
Address          string
CarNumbers       []string
AddressConfirmed bool
Identified       bool
Activated        bool
Lang             string
CreatedAt        timestamppb.Timestamp
UpdatedAt        timestamppb.Timestamp
```
##### Throws
- other errors of the database (provided as they are)


### DeleteUser
Call: `DeleteUser`

Metadata: `grpc-caller-service: <name of the caller>`
##### Request
```
Id int64
```

##### Response
```
Id               int64
Username         string optional
Telegramname     string
Firstname        string
Lastname         string optional
Phone            string
Address          string
CarNumbers       []string
AddressConfirmed bool
Identified       bool
Activated        bool
Lang             string
CreatedAt        timestamppb.Timestamp
UpdatedAt        timestamppb.Timestamp
```
##### Throws
- other errors of the database (provided as they are)


---

## Events

The events are completely handled by OMS. Though any driver may be used as storage for the events.

Broker       | Implementation
-------------|---------------
MemoryBroker | The events are stored in-memory and dispatched to `ClientEventBuffer` that has also in-memory implementation

Currently the `MemoryBroker` is used.

Such gRPC calls are available:

Name         | Description                                                 |
-------------|-------------------------------------------------------------|
CreateClient | Registers the client in broker and creates it.              |
Subscribe    | Bi-directional stream to receive events.                    |
Emit         | Send an event to the broker to dispatch it to all services. |

### Calls

#### Create Client
Call: `CreateClient`
Use this call to register the client in broker and create it inside the OMS.

The request data consists of the name of the client that should be unique in
the context of the system. But it is acceptable to have several clients to use
the same name. In that case they would receive the same events they have subscribed to.
```proto
message CreateClientRequest {
  string name = 1;
}
```

The response consists of the name of the registered client and its status:

Status    | Description
----------|--------------------------------------------
`created` | The client has been registered and created
`exists`  | The client is already registered before

```proto
message CreateClientResponse {
  string name = 1;
  string status = 2;
}
```

##### Notes
- If the client already exists the call does not throw an error. So it is save to call it several times.


#### Subscribe
Call: `Subscribe`

A bi-directional stream.

In request you can specify the set of topics to subscribe to, as at the stage of creation of the client
in the call `CreateClient` you cannot do that. Also give the name of the client. You can change the
set of topics or client name any time you want.

If the client is not registered this call registers it itself without any throw of the error.

This call preserves the events if the registered client disconnects.
The preserved events are sent to the client when the connection is established again.
But the preserved events are stored by the set of topics the client was subscribed to before the disconnect.

```proto
message SubscribeRequest {
  repeated string topic = 1;
  string client_name = 2;
}

message SubscribeResponse {
  string topic = 1;
  string data = 2;
}
```

#### Push
Call: `Push`
Use this call to send an event. Specify the topic and the data to send.

```proto
message PushRequest {
  string topic = 1;
  string data = 2;
}

message PushResponse {
  string topic = 1;
}
```

